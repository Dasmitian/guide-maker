<?php
$response = array();

include 'db_connect.php';
include 'db_functions.php';
 
//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array
 
//Check for Mandatory parameters
if(isset($input['username']) && isset($input['password']) && isset($input['email'])){
	$username = $input['username'];
	$password = $input['password'];
	$email = $input['email'];
	
	//Check if user already exist
	if(!userExists($username)){
		$creationDate = date("Y-m-d H:i:s");
		//Query to register new user
		$insertQuery  = "INSERT INTO users(user_name, user_email, user_password, creation_date) VALUES (?,?,?,?)";
		if($stmt = $con->prepare($insertQuery)){
			$stmt->bind_param("ssss",$username,$email,$password,$creationDate);
			$stmt->execute();
			$response["status"] = 0;
			$response["message"] = "User created";
			$stmt->close();
		}
	}
	else{
		$response["status"] = 1;
		$response["message"] = "User exists";
	}
}
else{
	$response["status"] = 2;
	$response["message"] = "Missing mandatory parameters";
}
echo json_encode($response);
?>