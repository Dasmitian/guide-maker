<?php
$response = array();

include 'db_connect.php';
include 'db_functions.php';
 
//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

//Check for Mandatory parameters
if(isset($input['poi']) && isset($input['guidename'])){
	$touring_time = $input['touring_time'];
	$poi_name = $input['poi'];
	$poi_lat = $input['poi_lat'];
	$poi_long = $input['poi_long'];
	$map_file_name = $input['map_file_name'];
	$username = $input['username'];
	$user_id = getUserId($username);
	$guidename = $input['guidename'];
	$guide_id = getGuideId($guidename, $user_id);
	
	
	//Check if user already exist
	if(guideExists($guidename, $user_id)){
		//Query to register new user
		$insertQuery  = "INSERT INTO interior_guides(touring_time, poi_name, poi_lat, poi_long, map_file_name, guide_id) VALUES (?,?,?,?,?,?)";
		if($stmt = $con->prepare($insertQuery)){
			$stmt->bind_param("ssddsi",$touring_time, $poi_name, $poi_lat, $poi_long, $map_file_name, $guide_id);
			if($stmt->execute()){
				$response["status"] = 0;
				$response["message"] = "Guide added to list";
			} else {
				$response["status"] = 2;
				$response["message"] = $touring_time . "+" . $poi_name . "+" . $poi_lat . "+" . $poi_long . "+" . $map_file_name . "+" . $guide_id;
			}
			$stmt->close();
		}
	}
	else{
		$response["status"] = 1;
		$response["message"] = "Guide with this name not found";
	}
}
else{
	$response["status"] = 2;
	$response["message"] = "Missing mandatory parameters";
}

echo json_encode($response);
?>