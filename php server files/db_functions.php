<?php
$random_salt_length = 32;

function userExists($username){
	$query = "SELECT user_name FROM users WHERE user_name = ?";
	global $con;
	if($stmt = $con->prepare($query)){
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$stmt->store_result();
		$stmt->fetch();
		if($stmt->num_rows == 1){
			$stmt->close();
			return true;
		}
		$stmt->close();
	}
	return false;
}

function guideExists($guidename, $user_id){
	$query = "SELECT guide_title FROM guides WHERE guide_title = ? AND user_id = ?";
	global $con;
	if($stmt = $con->prepare($query)){
		$stmt->bind_param("si", $guidename, $user_id);
		$stmt->execute();
		$stmt->store_result();
		$stmt->fetch();
		if($stmt->num_rows == 1){
			$stmt->close();
			return true;
		}
		$stmt->close();
	}
	return false;
}

function getUserId($username){
	$query = "SELECT user_id FROM users WHERE user_name = ?";
	global $con;
	if($stmt = $con->prepare($query)){
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$stmt->bind_result($user_id);
		$stmt->fetch();
		$stmt->close();
		return $user_id;
	}
	$stmt->close();
	return false;
}

function getGuideId($guidename, $user_id){
	$query = "SELECT guide_id FROM guides WHERE guide_title = ? AND user_id = ?";
	global $con;
	if($stmt = $con->prepare($query)){
		$stmt->bind_param("si", $guidename, $user_id);
		$stmt->execute();
		$stmt->bind_result($guide_id);
		$stmt->fetch();
		$stmt->close();
		return $guide_id;
	}
	$stmt->close();
	return false;
}


function interiorGuideExists($guidename){
	$query = "SELECT guidename FROM interior_guides WHERE guidename = ?";
	global $con;
	if($stmt = $con->prepare($query)){
		$stmt->bind_param("s", $guidename);
		$stmt->execute();
		$stmt->store_result();
		$stmt->fetch();
		if($stmt->num_rows == 1){
			$stmt->close();
			return true;
		}
		$stmt->close();
	}
	return false;
}
function getSalt(){
	global $random_salt_length;
	return bin2hex(openssl_random_pseudo_bytes($random_salt_length));
}

function concatPasswordWithSalt($passowrd, $salt){
	global $random_salt_length;
	if(random_salt_length % 2 == 0){
		$mid = $random_salt_length / 2;
	} else {
		$mid = ($random_salt_length - 1) / 2;
	}
	
	return substr($salt, 0, $mid - 1).$password.substr($salt, $mid, $random_salt_length - 1);
}
?>