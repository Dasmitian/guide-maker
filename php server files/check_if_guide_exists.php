<?php
$response = array();

include 'db_connect.php';
include 'db_functions.php';
 
//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array
 
//Check for Mandatory parameters
if(isset($input['title'])){
	$guidename = $input['title'];
	
	if(!guideExists($guidename)){	
		$response["status"] = 0;
		$response["message"] = "Guide name avaliable";
	}
	else{
		$response["status"] = 1;
		$response["message"] = "Guide with this name already exists";
	}
}
else{
	$response["status"] = 2;
	$response["message"] = "Missing mandatory parameters";
}
echo json_encode($response);
?>