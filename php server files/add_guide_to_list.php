<?php
$response = array();

include 'db_connect.php';
include 'db_functions.php';
 
//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array
 
 
if(isset($input['username']) && isset($input['title']) && isset($input['guideType'])){
	$username = $input['username'];
	$guide_title = $input['title'];
	$guide_type = $input['guideType'];
	$description = $input['description'];
	$image_file_path = $input['imageFilePath'];
	$creation_timestamp = $input['creationTimestamp'];
	$user_id = getUserId($username);
	
	if(!guideExists($guide_title)){
		$insertQuery  = "INSERT INTO guides(guide_title, description, image_file_path, guide_type, creation_timestamp, user_id) VALUES (?,?,?,?,?,?)";
		if($stmt = $con->prepare($insertQuery)){
			$stmt->bind_param("sssssi",$guide_title, $description, $image_file_name, $guide_type, $creation_timestamp, $user_id);
			if($stmt->execute()){
				$response["status"] = 0;
				$response["message"] = "Guide added to list";
			} else {
				$response["status"] = 2;
				$response["message"] = $user_id;
			}
			$stmt->close();
		}
	} else{
		$response["status"] = 1;
		$response["message"] = "Guide with this name already exists";
	}
} else{
	$response["status"] = 2;
	$response["message"] = "Missing mandatory parameters";
}
echo json_encode($response);
?>