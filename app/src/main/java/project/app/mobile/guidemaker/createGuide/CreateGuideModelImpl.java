package project.app.mobile.guidemaker.createGuide;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.SessionHandler;
import project.app.mobile.guidemaker.User;
import project.app.mobile.guidemaker.Languages;

public class CreateGuideModelImpl implements CreateGuideContract.CreateGuideModel {

    private static final String KEY_STATUS = "status";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_IMAGE_FILE_NAME = "imageFileName";
    private static final String KEY_GUIDE_TYPE = "guideType";
    private static final String KEY_CREATION_TIMESTAMP = "creationTimestamp";
    private static final String KEY_USERNAME = "username";
    private String addGuideToList_url = "http://guidemaker.gearhostpreview.com/php/add_guide_to_list.php";
    private String checkIfGuideToList_url = "http://guidemaker.gearhostpreview.com/php/check_if_guide_exists.php";

    private String username;
    private SessionHandler session;
    private String previewImageName = "previewImage.jpg";
    private User user;
    private LocalSqliteDb localDb;
    private HashMap<String, String> guideData;
    private HashMap<String, String> guideTranslations;
    private HashMap interiorData;
    private ArrayList<HashMap> interiorObjectData;
    private Languages languages;
    private int guideId;
    private boolean itemsSwapped;

    private CreateGuideContract.CreateGuidePresenterOps presenter;

    public CreateGuideModelImpl( CreateGuideContract.CreateGuidePresenterOps presenter, Context context){
        this.presenter = presenter;
        session = new SessionHandler(context);
        user = session.getUserDetails();
        guideData = new HashMap<>();
        guideTranslations = new HashMap<>();
        interiorData = new HashMap();
        interiorObjectData = new ArrayList<>();
        languages = new Languages();
        createTablesInLocalDb(context);
    }

    @Override
    public void setGuideData(String guideTitle, String imageName, String guideDescription, String guideType, String language){
        guideData.put("username", getUsername());
        guideData.put("imageName", imageName);
        guideData.put("guideType", guideType);
        guideTranslations.put("guideTitle", guideTitle);
        guideTranslations.put("originalGuideTitle", guideTitle);
        guideTranslations.put("guideDescription", guideDescription);
        guideTranslations.put("language", language);
    }

    public HashMap getInteriorData() {
        return interiorData;
    }

    public void setInteriorData(HashMap interiorData) {
        this.interiorData = interiorData;
    }

    public ArrayList<HashMap> getInteriorObjectData() {
        return interiorObjectData;
    }

    public void setInteriorObjectData(ArrayList<HashMap> interiorObjectData, boolean itemsSwapped) {
        this.interiorObjectData = interiorObjectData;
        this.itemsSwapped = itemsSwapped;
    }

    @Override
    public void createTablesInLocalDb(Context context){
        localDb = new LocalSqliteDb(context, "localDatabase.db");
        localDb.createLocalDbTables();
    }

    @Override
    public void addGuideToLocalDb(){
        guideId = localDb.insertGuideToLocalDb(guideData);
        localDb.insertGuideTranslationToLocalDb(guideTranslations, guideId);
    }

    @Override
    public void addInteriorToLocalDb(){
        localDb.insertInteriorToLocalDb(interiorData, guideId);
    }

    @Override
    public void addInteriorObjectsToLocalDb(){
        localDb.insertInteriorObjectsTranslationsToLocalDb(interiorObjectData, guideId);
    }

    public void updateGuideInLocalDb(int editedGuideId){
        localDb.updateGuideInLocalDb(guideData, editedGuideId);
        localDb.updateGuideTranslationsInLocalDb(guideTranslations, editedGuideId);
    }

    public void updateInteriorInLocalDb(int editedGuideId){
        localDb.updateInteriorInLocalDb(interiorData, editedGuideId);
    }

    public void updateInteriorObjectsInLocalDb(int editedGuideId){
        if(itemsSwapped) {
            localDb.updateInteriorObjectsInLocalDb(interiorObjectData, editedGuideId);
        } else {
            localDb.manageInteriorObjectsInLocalDb(interiorObjectData, editedGuideId);
        }
    }

    @Override
    public void addGuideToList(String title, String description, String guideType) {
        username = user.getUsername();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String creationTimestamp = dateFormat.format(new Date());
        JSONObject request = new JSONObject();
        try {
            request.put(KEY_USERNAME, username);
            request.put(KEY_TITLE, title);
            request.put(KEY_DESCRIPTION, description);
            request.put(KEY_IMAGE_FILE_NAME, previewImageName);
            request.put(KEY_GUIDE_TYPE, guideType);
            request.put(KEY_CREATION_TIMESTAMP, creationTimestamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjectRequest = new JsonObjectRequest(Request.Method.POST, addGuideToList_url, request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt(KEY_STATUS) == 0) {
                        presenter.showToastMessage(response.getString(KEY_MESSAGE));
                    } else if (response.getInt(KEY_STATUS) == 1) {
                        presenter.onGuideExists("Guide with that name exists");
                    } else {
                        presenter.showToastMessage(response.getString(KEY_MESSAGE));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                presenter.showToastMessage(error.getMessage());
            }
        });
        presenter.passJsObjectRequest(jsObjectRequest);
    }

    @Override
    public void checkGuideTitleInDB(String title){
        JSONObject request = new JSONObject();
        try{
            request.put(KEY_TITLE, title);
        } catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest jsObjectRequest = new JsonObjectRequest(Request.Method.POST, checkIfGuideToList_url, request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt(KEY_STATUS) == 0) {
                        //presenter.setTitleAvailability(true);
                        presenter.showToastMessage(response.getString(KEY_MESSAGE));
                    } else if(response.getInt(KEY_STATUS) == 1){
                        //presenter.setTitleAvailability(false);
                        presenter.showToastMessage(response.getString(KEY_MESSAGE));
                    } else {
                        presenter.showToastMessage(response.getString(KEY_MESSAGE));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                presenter.showToastMessage(error.getMessage());
            }
        });
        presenter.passJsObjectRequest(jsObjectRequest);
    }

    public String getUsername() {
        username = user.getUsername();
        return username;
    }

    public String getGuideTitleFromLocalDb(int editedGuideId){
        return  localDb.getTitleFromLocalDb(editedGuideId);
    }

    public String getPreviewImageNameFromLocalDb(int editedGuideId){
        return localDb.getPreviewImageNameFromLocalDb(editedGuideId);
    }
}
