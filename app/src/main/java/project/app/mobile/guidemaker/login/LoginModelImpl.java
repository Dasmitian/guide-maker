package project.app.mobile.guidemaker.login;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginModelImpl implements LoginContract.LoginModel {

    private static final String KEY_STATUS = "status";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";
    private String email = "";
    private String login_url = "http://guidemaker.gearhostpreview.com/php/login.php";

    private LoginContract.RequiredLoginPresenterOps presenter;

    public LoginModelImpl(LoginContract.RequiredLoginPresenterOps presenter){
        this.presenter = presenter;
    }

    @Override
    public void login(final String username, String password) {
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put(KEY_USERNAME, username);
            request.put(KEY_PASSWORD, password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjectRequest = new JsonObjectRequest
                (Request.Method.POST, login_url, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Check if user got logged in successfully
                            if (response.getInt(KEY_STATUS) == 0) {
                                presenter.onLoginSuccess();
                            }else{
                                presenter.onLoginFail(response.getString(KEY_MESSAGE));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        presenter.onLoginErrorResponse(error.getMessage());
                    }
                });
        // Access the RequestQueue through your singleton class.
        presenter.passJsObjectRequest(jsObjectRequest);
    }
}
