package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.R;

public class ObjectMapsAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list;
    private String guideName;

    public ObjectMapsAdapter(ArrayList<String> list, String guideName){
        this.list = list;
        this.guideName = guideName;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if ( view == null ){
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.maps_list, null);
        }

        TextView mapsList = view.findViewById(R.id.map_item);
        mapsList.setText(list.get(position));

        Button deleteMapButton = view.findViewById(R.id.delete_map_button);

        deleteMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileManager fileManager = new FileManager();
                fileManager.deleteFile("GuideMaker", guideName, list.get(position));
                list.remove(position);
                notifyDataSetChanged();
            }
        });



        return view;
    }
}
