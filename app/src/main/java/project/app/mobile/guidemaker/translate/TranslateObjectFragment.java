package project.app.mobile.guidemaker.translate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;

public class TranslateObjectFragment extends Fragment {

    private HashMap objectData;
    private String translateTo;
    private LocalSqliteDb localDb;
    private FileManager fileManager;
    private static int REQUEST_LOAD_AUDIO = 2;
    private File audioFile;
    private boolean audioSelected = false;
    private String errorMessage;
    private int guideId;
    private String guideTitle;

    private TextView tvTranslateObjectAudio;
    private EditText etTranslateObjectName;
    private EditText etTranslateObjectDescription;

    private OnFragmentInteractionListener mListener;

    public TranslateObjectFragment() {
        // Required empty public constructor
    }

    public TranslateObjectFragment(HashMap objectData,  String translateTo, int guideId, String guideTitle) {
        this.objectData = objectData;
        this.translateTo = translateTo;
        this.guideId = guideId;
        this.guideTitle = guideTitle;
    }

    public static TranslateObjectFragment newInstance(String param1, String param2) {
        TranslateObjectFragment fragment = new TranslateObjectFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_translate_object, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        localDb = new LocalSqliteDb(getContext(), "localDatabase.db");
        fileManager = new FileManager();
        errorMessage = getResources().getString(R.string.add_poi_error_message);

        TextView tvOriginalObjectName = rootView.findViewById(R.id.originalObjectName);
        final TextView tvOriginalObjectDescription = rootView.findViewById(R.id.originalObjectDescription);
        TextView tvOriginalObjectAudio = rootView.findViewById(R.id.originalObjectAudio);
        etTranslateObjectName = rootView.findViewById(R.id.translateObjectName);
        etTranslateObjectDescription = rootView.findViewById(R.id.translateObjectDescription);
        tvTranslateObjectAudio = rootView.findViewById(R.id.translateObjectAudio);
        Button btnSelectTranslateObjectAudio = rootView.findViewById(R.id.translateObjectAudioButton);
        Button btnSaveTranslatedObject = rootView.findViewById(R.id.translateObjectSaveButton);

        tvOriginalObjectName.setText(objectData.get("objectName").toString());
        tvOriginalObjectDescription.setText(objectData.get("objectDescription").toString());
        tvOriginalObjectAudio.setText(objectData.get("audioFileName").toString());

        btnSelectTranslateObjectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_AUDIO);
            }
        });


        btnSaveTranslatedObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getEtTranslateObjectDescription().equals("") && getTvTranslateObjectAudio().equals("")){
                    tvTranslateObjectAudio.setError(errorMessage);
                    etTranslateObjectDescription.setError(errorMessage);
                }
                HashMap<String, String> translatedObjectData = new HashMap<>();
                translatedObjectData.put("objectName", getEtTranslateObjectName());
                translatedObjectData.put("objectDescription", getEtTranslateObjectDescription());
                translatedObjectData.put("audioFileName", getTvTranslateObjectAudio());
                translatedObjectData.put("imageFileName", objectData.get("imageFileName").toString());
                translatedObjectData.put("language", translateTo);
                localDb.insertInteriorObjectTranslationsToLocalDb(translatedObjectData, guideId);
                copyFiles();
                Toast.makeText(getActivity(), getResources().getString(R.string.object_added), Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    private String getEtTranslateObjectName(){
        return etTranslateObjectName.getText().toString();
    }

    private String getEtTranslateObjectDescription(){
        return etTranslateObjectDescription.getText().toString();
    }

    private String getTvTranslateObjectAudio(){
        return tvTranslateObjectAudio.getText().toString();
    }

    private void copyFiles(){
        File audio = new File( Environment.getExternalStorageDirectory() + "/GuideMaker/" + guideTitle, getTvTranslateObjectAudio());
        try {
            if(audioSelected) {
                fileManager.copyFile(audioFile, audio);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_LOAD_AUDIO && resultCode == Activity.RESULT_OK && null != data){
            Uri selectedAudio = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedAudio,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String audioPath = cursor.getString(columnIndex);
            audioFile = new File(audioPath);
            tvTranslateObjectAudio.setText(audioFile.getName());
            audioSelected = true;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
