package project.app.mobile.guidemaker.viewGuide;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.LocalSqliteDb;

public class ViewGuideModelImpl implements ViewGuideContract.viewGuideModel{

    private Context context;
    private ViewGuideContract.viewGuidePresenter presenter;
    private LocalSqliteDb localDb;

    public ViewGuideModelImpl(ViewGuideContract.viewGuidePresenter presenter, Context context){
        this.context = context;
        this.presenter = presenter;
        localDb = new LocalSqliteDb(context, "localDatabase.db");
    }

    public HashMap getGuideFromDb(int guideId){
        return localDb.getGuideFromLocalDb(guideId);
    }

    public HashMap getGuideTranslationsFromDb(int guideId, String language){
        if( language == null ){
            return localDb.getDefaultGuideTranslationsFromLocalDb(guideId);
        } else {
            return localDb.getGuideTranslationsFromLocalDb(guideId, language);
        }
    }

    public HashMap getInteriorFromDb(int guideId){
        HashMap interiorData = localDb.getInteriorFromLocalDb(guideId);
        return interiorData;
    }

    public ArrayList getInteriorObjectsFromDb(int guideId, String language){
        if(language == null){
            ArrayList interiorObjectsData = localDb.getInteriorObjectsFromLocalDb(guideId);
            return interiorObjectsData;
        } else {
            ArrayList interiorObjectsData = localDb.getInteriorObjectsFromLocalDb(guideId, language);
            return interiorObjectsData;
        }
    }

    public ArrayList getInteriorMaps(int guideId){
        HashMap interiorData = localDb.getInteriorFromLocalDb(guideId);
        ArrayList<String> mapsNamesList = (ArrayList<String>) interiorData.get("mapsNamesList");
        return mapsNamesList;
    }


}
