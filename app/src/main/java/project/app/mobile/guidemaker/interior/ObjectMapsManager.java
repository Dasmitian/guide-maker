package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectLanguageDialogFragment;
import project.app.mobile.guidemaker.SelectMapDialogFragment;
import project.app.mobile.guidemaker.SharedPreferencesClass;

public class ObjectMapsManager extends Fragment {

    private static int REQUEST_LOAD_IMAGE = 1;
    private File mapFile;
    private FileManager fileManager;
    private FragmentManager fragmentManager;

    private ObjectMapsAdapter adapter;
    private ListView mapsList;
    private SharedPreferencesClass preferences;

    private ArrayList<String> mapsNamesList;
    private String mainFolder = "GuideMaker";
    private String guideName;

    private OnFragmentInteractionListener mListener;

    public ObjectMapsManager() {
        // Required empty public constructor
    }

    public static ObjectMapsManager newInstance() {
        ObjectMapsManager fragment = new ObjectMapsManager();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_object_maps_manager, container, false);

        Button mapButton = rootView.findViewById(R.id.addMapButton);
        Button segmentButton = rootView.findViewById(R.id.addSegmentButton);
        fragmentManager = getFragmentManager();
        mapsList = rootView.findViewById(R.id.mapsList);
        mapsNamesList = new ArrayList<>();
        fileManager = new FileManager();
        preferences = new SharedPreferencesClass(getActivity());
        guideName = preferences.getGuideName();
        createList();

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_IMAGE);
            }
        });

        segmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mapsNamesList.size() != 0) {
                    Fragment selectMapDialogFragment = new SelectMapDialogFragment().newInstance(mapsNamesList, guideName);
                    fragmentManager.beginTransaction().replace(R.id.objectMapsManager, selectMapDialogFragment).addToBackStack(null).commit();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.maps_missing), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == REQUEST_LOAD_IMAGE && resultCode == getActivity().RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            mapFile = new File(picturePath);
            mapsNamesList.add(mapFile.getName());
            copyMapFile(mapFile);
            createList();
        }
    }

    public void createList(){
        adapter = new ObjectMapsAdapter(mapsNamesList, guideName);
        mapsList.setAdapter(adapter);
    }

    public void copyMapFile(File mapFile){
        File map = new File(Environment.getExternalStorageDirectory() + "/" + mainFolder + "/" + guideName, mapFile.getName());
        try {
            fileManager.copyFile(mapFile, map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
