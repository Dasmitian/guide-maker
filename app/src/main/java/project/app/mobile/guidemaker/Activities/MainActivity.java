package project.app.mobile.guidemaker.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import project.app.mobile.guidemaker.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
