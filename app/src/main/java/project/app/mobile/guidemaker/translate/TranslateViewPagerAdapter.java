package project.app.mobile.guidemaker.translate;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class TranslateViewPagerAdapter extends FragmentPagerAdapter {

    private int numberOfTabs;
    private int guideId;
    private ArrayList<HashMap<String, String>> interiorObjectsDataList;
    private HashMap guideTranslationsData;
    private String guideTitle;
    private String translateTo;

    public TranslateViewPagerAdapter(FragmentManager fm, int numberOfTabs, ArrayList<HashMap<String, String>> interiorObjectsDataList, HashMap guideTranslationsData, String guideTitle, String translateTo, int guideId) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
        this.interiorObjectsDataList = interiorObjectsDataList;
        this.guideTranslationsData = guideTranslationsData;
        this.guideTitle = guideTitle;
        this.guideId = guideId;
        this.translateTo = translateTo;
    }


    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            TranslateGuideFragment fragment = new TranslateGuideFragment(guideTranslationsData, translateTo, guideId);
            return fragment;
        }
        TranslateObjectFragment fragment = new TranslateObjectFragment(interiorObjectsDataList.get(position-1), translateTo, guideId, guideTitle);
        return fragment;
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
