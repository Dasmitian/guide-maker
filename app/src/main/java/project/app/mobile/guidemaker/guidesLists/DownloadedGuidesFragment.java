package project.app.mobile.guidemaker.guidesLists;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.Languages;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectLanguageDialogFragment;
import project.app.mobile.guidemaker.viewGuide.ViewGuideActivity;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DownloadedGuidesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DownloadedGuidesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DownloadedGuidesFragment extends Fragment implements AdapterView.OnItemClickListener, SelectLanguageDialogFragment.OnLanguageSelectionListener {

    private ListView guidesList;
    private boolean isGuideListCreated = false;

    private LocalSqliteDb localDb;
    private ArrayList<HashMap> guideDataList;
    private ArrayList<Integer> guidesIdsByLanguage;
    private String chosenLanguage;
    private Languages languages;
    private ArrayList<String> supportedLanguages;
    private TextView tvChosenLanguage;

    private DownloadedGuidesListAdapter adapter;

    private OnFragmentInteractionListener mListener;

    public DownloadedGuidesFragment() {
        // Required empty public constructor
    }

    public static DownloadedGuidesFragment newInstance(String param1, String param2) {
        DownloadedGuidesFragment fragment = new DownloadedGuidesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localDb = new LocalSqliteDb(getActivity(), "localDatabase.db");
        languages = new Languages();
        supportedLanguages = languages.getSupportedLanguages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_local_guides, container, false);
        tvChosenLanguage = rootView.findViewById(R.id.chosenLanguage);
        Button btnChooseLanguage = rootView.findViewById(R.id.chooseLanguageButton);

        btnChooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showDialog();

            }
        });
        if( !isGuideListCreated ) {
            getList();
        }
        guidesList = rootView.findViewById(R.id.downloadedGuidesList);
        guidesList.setOnItemClickListener(this);
        createGuidesList();
        return rootView;
    }

    private void showDialog(){
        Fragment selectLanguageDialogFragment = new SelectLanguageDialogFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentLocalGuides, selectLanguageDialogFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //filter guides by language. If no guides with selected language are found, guides with is_default set to 1 are selected
    private void filterGuidesList(String chosenLanguage){
        guidesIdsByLanguage = localDb.getGuidesIdsByLanguage(chosenLanguage);
        guideDataList = localDb.getGuideDataById(guidesIdsByLanguage);
        adapter.updateAdapter(guideDataList);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int guideId = (int) adapter.getItemId(position);
        String language = adapter.getItem(position);
        Intent intent = new Intent(getContext(), ViewGuideActivity.class);
        intent.putExtra("guideId", guideId);
        intent.putExtra("language", language);
        startActivity(intent);
    }

    @Override
    public void onLanguageSelection(String chosenLanguage) {
        tvChosenLanguage.setText(chosenLanguage);
        filterGuidesList(chosenLanguage);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void createGuidesList(){
        adapter = new DownloadedGuidesListAdapter(guideDataList, getContext());
        guidesList.setAdapter(adapter);
    }

    private void getList(){
        guideDataList = localDb.getGuideDataByDefaultLanguage();
        isGuideListCreated = true;
    }
}
