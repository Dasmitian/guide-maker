package project.app.mobile.guidemaker.translate;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import project.app.mobile.guidemaker.Activities.MainActivity;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
public class TranslateGuideFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private HashMap guideTranslationsData;
    private String translateTo;
    private int guideId;
    private LocalSqliteDb localDb;

    private EditText etTranslateGuideTitle;
    private EditText etTranslateGuideDescription;
    private TextView tvOriginalGuideTitle;

    public TranslateGuideFragment() {
        // Required empty public constructor
    }

    public TranslateGuideFragment(HashMap guideTranslationsData, String translateTo, int guideId){
        this.guideTranslationsData = guideTranslationsData;
        this.translateTo = translateTo;
        this.guideId = guideId;
    }

    public static TranslateGuideFragment newInstance() {
        TranslateGuideFragment fragment = new TranslateGuideFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_translate_guide, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        localDb = new LocalSqliteDb(getActivity(),"localDatabase.db");

        Button btnSave = rootView.findViewById(R.id.translateGuideSaveButton);
        tvOriginalGuideTitle = rootView.findViewById(R.id.originalGuideTitle);
        TextView tvOriginalGuideDescription = rootView.findViewById(R.id.originalGuideDescription);
        etTranslateGuideTitle = rootView.findViewById(R.id.translateGuideTitle);
        etTranslateGuideDescription = rootView.findViewById(R.id.translateGuideDescription);

        tvOriginalGuideTitle.setText(guideTranslationsData.get("guideTitle").toString());
        tvOriginalGuideDescription.setText(guideTranslationsData.get("guideDescription").toString());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> guideData = new HashMap<>();
                guideData.put("guideTitle", getEtTranslateGuideTitle());
                guideData.put("guideDescription", getEtTranslateGuideDescription());
                guideData.put("originalGuideTitle", getTvOriginalGuideTitle());
                guideData.put("language", translateTo);
                localDb.insertGuideTranslationToLocalDb(guideData, guideId);
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.saved), Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    private String getEtTranslateGuideTitle(){
        return etTranslateGuideTitle.getText().toString();
    }

    private String getEtTranslateGuideDescription(){
        return etTranslateGuideDescription.getText().toString();
    }

    private String getTvOriginalGuideTitle(){
        return tvOriginalGuideTitle.getText().toString();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
