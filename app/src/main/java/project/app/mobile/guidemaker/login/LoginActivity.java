package project.app.mobile.guidemaker.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.JsonObjectRequest;
import project.app.mobile.guidemaker.register.RegisterActivity;
import project.app.mobile.guidemaker.MySingleton;
import project.app.mobile.guidemaker.NavDrawerGuest;
import project.app.mobile.guidemaker.NavDrawerUser;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SessionHandler;

public class LoginActivity extends AppCompatActivity implements LoginContract.LoginView {
    private EditText etUsername;
    private EditText etPassword;
    private SessionHandler session;
    private ProgressDialog pDialog;
    private LoginPresenterImpl presenter;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionHandler(getApplicationContext());
        presenter = new LoginPresenterImpl(this, session);
        initProgressDialog();
        if(session.isLoggedIn()){
            loadNavDrawerUser();
        }
        etUsername = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.etLoginPassword);

        Button login = findViewById(R.id.sign_in_button);
        Button register = findViewById(R.id.register_button);

        register.setOnClickListener(new View.OnClickListener(){
            @Override
                public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                pDialog.show();
                username = getEtUsername();
                password = getEtPassword();
                if ( presenter.validateInputs(username, password ) == 0 ) {
                    presenter.loginUser();
                } else if (presenter.validateInputs(username, password ) ==  1 ){
                    pDialog.hide();
                    etUsername.setError("Username cannot be empty");
                    etUsername.requestFocus();
                } else {
                    pDialog.hide();
                    etPassword.setError("Password cannot be empty");
                    etPassword.requestFocus();
                }
            }
        });
    }

    @Override
    public String getEtUsername() {
        return etUsername.getText().toString().toLowerCase().trim();
    }

    @Override
    public String getEtPassword() {
        return etPassword.getText().toString().trim();
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(getApplicationContext(), NavDrawerGuest.class);
        startActivity(i);
        finish();
    }

    @Override
    public void loadNavDrawerUser() {
        if( pDialog.isShowing()) {
            pDialog.hide();
            pDialog.dismiss();
        }
        Intent i = new Intent(this, NavDrawerUser.class);
        startActivity(i);
        finish();
    }

    @Override
    public void showToastMessage(String message) {
        pDialog.hide();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initProgressDialog() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Logging in.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
    }

    @Override
    public void accessSingleton(JsonObjectRequest jsObjectRequest){
        MySingleton.getInstance(this).addToRequestQueue(jsObjectRequest);
    }
}