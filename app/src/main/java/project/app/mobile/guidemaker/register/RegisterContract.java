package project.app.mobile.guidemaker.register;

import com.android.volley.toolbox.JsonObjectRequest;

public interface RegisterContract {

    interface RegisterView {
        void initProgressDialog();
        String getEtUsername();
        void setEtUsername(String etUsernameMessage);
        String getEtPassword();
        String getEtEmail();
        String getEtConfirmPassword();
        void loadLoginActivity();
        void showToastMessage(String message);
        void accessSingleton(JsonObjectRequest jsObjectRequest);
    }

    interface RegisterPresenter {
        void registerUser();
        int validateInputs(String username, String password, String email, String confirmPassword);
    }

    interface RegisterPresenterOps {
        void onRegisterSuccess(String message);
        void onUsernameTaken(String message);
        void onRegisterFail();
        void passJsObjectRequest(JsonObjectRequest jsObjectRequest);
    }

    interface RegisterModel {
        void register(String username, String password, String email);
    }
}
