package project.app.mobile.guidemaker.login;

import com.android.volley.toolbox.JsonObjectRequest;

public interface LoginContract {

    interface LoginPresenter {
        void loginUser();
        int validateInputs(String username, String password);
    }

    interface RequiredLoginPresenterOps{
        void onLoginSuccess();
        void onLoginFail(String message);
        void onLoginErrorResponse(String message);
        void passJsObjectRequest(JsonObjectRequest jsObjectRequest);
    }

    interface LoginView {
        void initProgressDialog();
        void accessSingleton(JsonObjectRequest jsObjectRequest);
        String getEtUsername();
        String getEtPassword();
        void loadNavDrawerUser();
        void showToastMessage(String message);
    }

    interface LoginModel {
        void login(String username, String password);
    }
}
