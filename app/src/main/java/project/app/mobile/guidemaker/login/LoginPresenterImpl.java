package project.app.mobile.guidemaker.login;


import com.android.volley.toolbox.JsonObjectRequest;
import project.app.mobile.guidemaker.SessionHandler;

public class LoginPresenterImpl implements LoginContract.LoginPresenter, LoginContract.RequiredLoginPresenterOps {

    private static final String KEY_EMPTY = "";

    private LoginContract.LoginView loginView;
    private LoginContract.LoginModel loginModel;
    private SessionHandler session;
    private String username;
    private String password;

    public LoginPresenterImpl(LoginContract.LoginView loginView, SessionHandler session){
        this.session = session;
        this.loginView = loginView;
        this.loginModel = new LoginModelImpl(this);
    }

    @Override
    public void loginUser() {
        username = loginView.getEtUsername();
        password = loginView.getEtPassword();
        loginModel.login(username, password);
    }

    @Override
    public int validateInputs(String username, String password) {
        if(KEY_EMPTY.equals(username)){
            return 1;
        }
        if(KEY_EMPTY.equals(password)){
            return 2;
        }
        return 0;
    }

    @Override
    public void onLoginSuccess() {
        session.loginUser(username);
        loginView.loadNavDrawerUser();
    }

    @Override
    public void onLoginFail(String message) {
        loginView.showToastMessage(message);
    }

    @Override
    public void onLoginErrorResponse(String message) {
        loginView.showToastMessage(message);
    }

    @Override
    public void passJsObjectRequest(JsonObjectRequest jsObjectRequest){
        loginView.accessSingleton(jsObjectRequest);
    }
}
