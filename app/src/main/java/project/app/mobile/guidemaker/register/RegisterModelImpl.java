package project.app.mobile.guidemaker.register;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;

import project.app.mobile.guidemaker.R;

public class RegisterModelImpl implements RegisterContract.RegisterModel {

    private static final String KEY_STATUS = "status";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private Context context;
    private String register_url = "http://guidemaker.gearhostpreview.com/php/register.php";

    private RegisterContract.RegisterPresenterOps presenter;

    RegisterModelImpl(RegisterContract.RegisterPresenterOps presenter, Context context){
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void register(String username, String password, String email){
        JSONObject request = new JSONObject();
        try {
            request.put(KEY_USERNAME, username);
            request.put(KEY_PASSWORD, password);
            request.put(KEY_EMAIL, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjectRequest = new JsonObjectRequest(Request.Method.POST, register_url, request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt(KEY_STATUS) == 0) {
                        presenter.onRegisterSuccess(KEY_MESSAGE);
                    } else if (response.getInt(KEY_STATUS) == 1) {
                        presenter.onUsernameTaken(context.getResources().getString(R.string.username_taken));
                    } else {
                        presenter.onRegisterFail();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                presenter.onRegisterFail();
            }
        });
        presenter.passJsObjectRequest(jsObjectRequest);
    }
}
