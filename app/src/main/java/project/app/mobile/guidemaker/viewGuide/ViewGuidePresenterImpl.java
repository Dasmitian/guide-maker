package project.app.mobile.guidemaker.viewGuide;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.GoogleMapsActivity;

public class ViewGuidePresenterImpl implements ViewGuideContract.viewGuidePresenter {

    private Context context;
    private ViewGuideContract.viewGuideView viewGuideView;
    private ViewGuideContract.viewGuideModel viewGuideModel;

    private String imagePath;
    private int rotation;
    private FileManager fileManager = new FileManager();
    private File imageFile;
    private ArrayList<String> mapsNamesList;
    private String originalGuideTitle;

    private double poiLat;
    private double poiLong;

    public ViewGuidePresenterImpl(ViewGuideContract.viewGuideView viewGuideView, Context context){
        this.context = context;
        this.viewGuideView = viewGuideView;
        this.viewGuideModel = new ViewGuideModelImpl(this, context);
    }

    public void loadGuideData(int guideId, String language){
        HashMap guideData = viewGuideModel.getGuideFromDb(guideId);
        HashMap guideDataTranslations = viewGuideModel.getGuideTranslationsFromDb(guideId, language);
        HashMap interiorData = viewGuideModel.getInteriorFromDb(guideId);

        String guideTitle = String.valueOf(guideDataTranslations.get("guideTitle"));
        originalGuideTitle = String.valueOf(guideDataTranslations.get("originalGuideTitle"));
        String guideDescription = String.valueOf(guideDataTranslations.get("guideDescription"));
        String previewImageName = String.valueOf(guideData.get("previewImageName"));

        viewGuideView.setTvGuideTitle(guideTitle);
        viewGuideView.setTvDescription(guideDescription);
        showPreviewImage(originalGuideTitle, previewImageName);

        viewGuideView.setTvTouringTime(String.valueOf(interiorData.get("touringTime")));
        poiLat = (double) interiorData.get("poiLat");
        poiLong = (double) interiorData.get("poiLong");
        mapsNamesList = (ArrayList<String>) interiorData.get("mapsNamesList");

    }

    private void showPreviewImage(String guideTitle, String imageFileName){
        imagePath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + guideTitle + "/" + imageFileName;
        rotation = fileManager.getExifOrientation(imagePath);
        imageFile = new File(imagePath);
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        viewGuideView.setIvPreviewImage(fileManager.loadImage(context, rotation, bitmap));
    }

    public void openMap(){
        Intent intent = new Intent(context, GoogleMapsActivity.class);
        intent.putExtra("poiLat", poiLat);
        intent.putExtra("poiLong", poiLong);
        context.startActivity(intent);
    }

    public void showObjectsAsList(int guideId, String language){
        ArrayList<HashMap<String, String>> interiorObjectsData = viewGuideModel.getInteriorObjectsFromDb(guideId, language);
        Intent intent = new Intent(context, ListObjects.class);
        intent.putExtra("interiorObjectDataList", interiorObjectsData);
        intent.putExtra("guideTitle", viewGuideView.getGuideTitle());
        intent.putExtra("originalGuideTitle", originalGuideTitle);
        intent.putExtra("guideId", guideId);
        if(language == null) {
            intent.putExtra("language", interiorObjectsData.get(0).get("language"));
        } else {
            intent.putExtra("language", language);
        }
        context.startActivity(intent);
    }

    public void showObjectsAsGrid(int guideId, String language){
        ArrayList<HashMap<String, String>> interiorObjectsData = viewGuideModel.getInteriorObjectsFromDb(guideId, language);
        Intent intent = new Intent(context, GridObjects.class);
        intent.putExtra("interiorObjectDataList", interiorObjectsData);
        intent.putExtra("guideTitle", viewGuideView.getGuideTitle());
        intent.putExtra("originalGuideTitle", originalGuideTitle);
        intent.putExtra("guideId", guideId);
        if(language == null) {
            intent.putExtra("language", interiorObjectsData.get(0).get("language"));
        } else {
            intent.putExtra("language", language);
        }
        context.startActivity(intent);
    }

    public void showMaps(int guideId){
        ArrayList<String> mapsNamesList = viewGuideModel.getInteriorMaps(guideId);
        Intent intent = new Intent(context, ShowMaps.class);
        intent.putExtra("mapsNamesList", mapsNamesList);
        intent.putExtra("originalGuideTitle", originalGuideTitle);
        context.startActivity(intent);
    }
}
