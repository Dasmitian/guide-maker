package project.app.mobile.guidemaker;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesClass {
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    private static final String KEY_USERNAME = "username";
    private static final String KEY_EXPIRES = "expires";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_EMPTY = "";
    private static final String KEY_GUIDE_NAME = "guideName";

    public SharedPreferencesClass(Context context){
        this.sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public void saveUser(String username, long millis){
        editor.putString(KEY_USERNAME, username);
        editor.putLong(KEY_EXPIRES, millis);
        editor.commit();
    }

    public void logoutUser(){
        editor.remove(KEY_USERNAME);
        editor.remove(KEY_EXPIRES);
        editor.commit();
    }

    public void setGuideName(String guideName){
        editor.putString(KEY_GUIDE_NAME, guideName);
        editor.commit();
    }

    public String getGuideName(){
        return sharedPreferences.getString(KEY_GUIDE_NAME, "");
    }

    public long getExpireTime(){
        return sharedPreferences.getLong(KEY_EXPIRES, 0);
    }

    public String getUsername(){
        return  sharedPreferences.getString(KEY_USERNAME, KEY_EMPTY);
    }

    public String getUserEmail(){
        return sharedPreferences.getString(KEY_EMAIL, KEY_EMPTY);
    }

}
