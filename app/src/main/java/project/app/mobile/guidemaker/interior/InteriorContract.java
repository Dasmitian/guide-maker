package project.app.mobile.guidemaker.interior;

import com.android.volley.toolbox.JsonObjectRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface InteriorContract {

    interface InteriorView {
        boolean checkReadStoragePermission();
        void setPoiInfo(String location, String placeId, double longitude, double latitude);
        void accessSingleton(JsonObjectRequest jsObjectRequest);
        void showToastMessage(String message);
        String getTvLocation();
        String getEtTouringTime();
        ArrayList<String> getMapsNamesList();
        void setMapsNamesList(ArrayList<String> mapsNames);
        void setTvLocation(String location);
        void setEtTouringTime(String touringTime);
        void startPoiFragment();
        void editPoiFragment(int position);
        void deleteObjectFromArray(int position);
        void createObjectsList();
    }

    interface InteriorModel {
        void addInteriorGuideToList(String touringTime, String guidename);
        void addPoiToInteriorDataMap(String poi, String poiId, double poiLongitude, double poiLatitude);
        void addInteriorData(String touringTime, ArrayList<String> mapsNamesList);
        void setInteriorData(HashMap data);
        HashMap getInteriorData();
        HashMap getObjectFromArrayList(int index);
        void addObjectToArray(HashMap objectData);
        void editObjectInArray(int index, HashMap objectData);
        void deleteObjectFromArray(int index);
        void setObjectsList(ArrayList<HashMap<String, String>> data);
        ArrayList getObjectsArrayList();
        void swapItemsInArray(int item1, int item2);
    }

    interface InteriorPresenter {
        void passPoiInfo(String location, String placeId, double longitude, double latitude);
        int getExifOrientation(String imagePath);
        void loadInteriorData(HashMap data);
        void addObjectToList(HashMap objectData);
        void deleteObjectFromArray(int index);
        void loadInteriorObjectsData(ArrayList<HashMap<String, String>> data);
    }

    interface InteriorPresenterOps {
        void passJsObjectRequest(JsonObjectRequest jsObjectRequest);
        void onAddingSuccess(String message);
        void onAddingFail(String message);
    }
}
