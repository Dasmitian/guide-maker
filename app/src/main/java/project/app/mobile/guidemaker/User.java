package project.app.mobile.guidemaker;

import java.util.Date;

public class User {

    String username;
    String userEmail;
    Date sessionExpiryDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setSessionExpiryDate(Date sessionExpiryDate){
        this.sessionExpiryDate = sessionExpiryDate;
    }

    public Date getSessionExpiryDate(){
        return  sessionExpiryDate;
    }

}
