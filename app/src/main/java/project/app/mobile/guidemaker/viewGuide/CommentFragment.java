package project.app.mobile.guidemaker.viewGuide;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SessionHandler;
import project.app.mobile.guidemaker.SharedPreferencesClass;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CommentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class CommentFragment extends Fragment {

    private int rating = 5;
    private int guideId;
    private LocalSqliteDb localDb;
    private SharedPreferencesClass preferences;
    private String username;
    private boolean userCommented = false;
    private HashMap commentData;
    private EditText etComment;
    private RadioGroup radioGroup;
    private RadioButton radioButton5;
    private Button btnSubmit;
    private SessionHandler session;
    private ConstraintLayout loggedInLayout;
    private ConstraintLayout loggedOutLayout;



    private OnFragmentInteractionListener mListener;

    public CommentFragment() {
        // Required empty public constructor
    }

    public CommentFragment(int guideId){
        this.guideId = guideId;
    }

    public static CommentFragment newInstance() {
        CommentFragment fragment = new CommentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_comment, container, false);

        session = new SessionHandler(getActivity());
        localDb = new LocalSqliteDb(getActivity(), "localDatabase.db");
        preferences = new SharedPreferencesClass(getActivity());
        username = preferences.getUsername();

        loggedInLayout = rootView.findViewById(R.id.loggedInLayout);
        loggedOutLayout = rootView.findViewById(R.id.loggedOutLayout);
        etComment = rootView.findViewById(R.id.commentText);
        btnSubmit = rootView.findViewById(R.id.buttonSubmit);
        radioGroup = rootView.findViewById(R.id.commentRadioGroup);
        radioButton5 = rootView.findViewById(R.id.radioButton5);

        radioButton5.toggle();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch(i){
                    case R.id.radioButton1:
                        rating = 1;
                        break;
                    case R.id.radioButton2:
                        rating = 2;
                        break;
                    case R.id.radioButton3:
                        rating = 3;
                        break;
                    case R.id.radioButton4:
                        rating = 4;
                        break;
                    case R.id.radioButton5:
                        rating = 5;
                        break;
                }
            }
        });

        getUserComment();
        if( userCommented ){
            btnSubmit.setText(getResources().getString(R.string.update));
        } else {
            btnSubmit.setText(getResources().getString(R.string.submit));
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( !userCommented ) {
                    localDb.insertCommentToLocalDb(etComment.getText().toString(), rating, username, guideId);
                    userCommented = true;
                    btnSubmit.setText(getResources().getString(R.string.update));
                } else {
                    localDb.updateUserCommentInLocalDb(rating, etComment.getText().toString(), guideId, username);
                }
            }
        });

        if(session.isLoggedIn()){
            loggedInLayout.setVisibility(View.VISIBLE);
            loggedOutLayout.setVisibility(View.GONE);
        } else {
            loggedInLayout.setVisibility(View.GONE);
            loggedOutLayout.setVisibility(View.VISIBLE);

        }
        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void getUserComment(){
        commentData = new HashMap();
        commentData = localDb.getUserCommentFromLocalDb(username, guideId);
        if(commentData != null) {
            userCommented = true;
            etComment.setText(commentData.get("comment").toString());
            RadioButton button = (RadioButton) radioGroup.getChildAt
                    (Integer.parseInt(String.valueOf(commentData.get("rating")))-1);
            button.toggle();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onCommentRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.radioButton1:
                if(checked){
                    rating = 1;
                }
                break;
            case R.id.radioButton2:
                if(checked){
                    rating = 2;
                }
                break;
            case R.id.radioButton3:
                if(checked){
                    rating = 3;
                }
                break;
            case R.id.radioButton4:
                if(checked){
                    rating = 4;
                }
                break;
            case R.id.radioButton5:
                if(checked){
                    rating = 5;
                }
                break;
        }
    }
}
