package project.app.mobile.guidemaker;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

public class SessionHandler {
    SharedPreferencesClass preferences;

    public SessionHandler( Context context ){
        preferences = new SharedPreferencesClass(context);
    }

    public void loginUser(String username){
        Date date = new Date();
        long millis = date.getTime() + ( 7 * 24 * 60 * 60 * 1000);
        preferences.saveUser(username, millis);
    }

    public boolean isLoggedIn(){
        Date currentDate = new Date();
        long millis = preferences.getExpireTime();
        if( millis == 0){
            return false;
        }
        Date expiryDate = new Date(millis);
        return currentDate.before(expiryDate);
    }

    public User getUserDetails(){
        if(!isLoggedIn()){
            return  null;
        }
        User user = new User();
        user.setUsername(preferences.getUsername());
        user.setUserEmail(preferences.getUserEmail());
        user.setSessionExpiryDate(new Date(preferences.getExpireTime()));

        return user;
    }

    public void logoutUser(){
        preferences.logoutUser();
    }
}
