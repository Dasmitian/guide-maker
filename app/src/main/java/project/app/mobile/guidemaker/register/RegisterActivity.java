package project.app.mobile.guidemaker.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.JsonObjectRequest;
import project.app.mobile.guidemaker.MySingleton;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.login.LoginActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterContract.RegisterView {
    private EditText etUsername;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private EditText etEmail;
    private ProgressDialog pDialog;
    private String errorMessage;

    RegisterPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenterImpl(this, getApplicationContext());
        errorMessage = getResources().getString(R.string.error_field_required);

        etUsername = findViewById(R.id.etRegisterUsername);
        etPassword = findViewById(R.id.etRegisterPassword);
        etConfirmPassword = findViewById(R.id.etRegisterConfirmPassword);
        etEmail = findViewById(R.id.etRegisterEmail);

        Button register = findViewById(R.id.register_button);

        register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                initProgressDialog();
                if(presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 0){
                    presenter.registerUser();
                } else if( presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 1 ){
                    pDialog.hide();
                    etUsername.setError(errorMessage);
                    etUsername.requestFocus();
                } else if( presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 2 ){
                    pDialog.hide();
                    etEmail.setError(errorMessage);
                    etEmail.requestFocus();
                } else if( presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 3 ){
                    pDialog.hide();
                    etPassword.setError(errorMessage);
                    etPassword.requestFocus();
                } else if( presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 4 ){
                    pDialog.hide();
                    etConfirmPassword.setError(errorMessage);
                    etConfirmPassword.requestFocus();
                } else if( presenter.validateInputs(getEtUsername(), getEtPassword(), getEtEmail(), getEtConfirmPassword()) == 5 ){
                    pDialog.hide();
                    etConfirmPassword.setError("Password and Confirm Password does not match");
                    etConfirmPassword.requestFocus();
                }

            }
        });
    }

    @Override
    public String getEtUsername() {
        return etUsername.getText().toString().toLowerCase().trim();
    }

    public void setEtUsername(String etUsernameMessage) {
        etUsername.setError(etUsernameMessage);
        etUsername.requestFocus();
    }

    @Override
    public String getEtPassword() {
        return etPassword.getText().toString().trim();
    }

    @Override
    public String getEtConfirmPassword() {
        return etConfirmPassword.getText().toString().trim();
    }

    @Override
    public String getEtEmail() {
        return etEmail.getText().toString().trim();
    }

    @Override
    public void loadLoginActivity() {
        pDialog.hide();
        pDialog.dismiss();
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void initProgressDialog() {
        pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Registering.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void showToastMessage(String message) {
        pDialog.hide();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void accessSingleton(JsonObjectRequest jsObjectRequest){
        MySingleton.getInstance(this).addToRequestQueue(jsObjectRequest);
    }
}
