package project.app.mobile.guidemaker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;

import androidx.exifinterface.media.ExifInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileManager {

    public void copyFile(File sourceFile, File destinationFile) throws IOException {
        if(sourceFile != null) {
            if (!sourceFile.exists()) {
                return;
            }

            FileChannel source;
            FileChannel destination;
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destinationFile).getChannel();
            if (destination != null && source != null) {
                destination.transferFrom(source, 0, source.size());
            }
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    public Bitmap loadImage(Context context, int rotation, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        if (bitmap == null){
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_missing);
        }
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return bitmap;
    }

    public void deleteGuideFolder(String mainFolderName, String folderName){
        File guideFolder = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName, folderName);
        if( guideFolder.exists() ) {
            for (File file : guideFolder.listFiles()) {
                file.delete();
            }
            guideFolder.delete();
        }
    }

    public void deleteFile(String mainFolderName, String folderName, String fileName){
        File file = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName + "/" + folderName, fileName);
        file.delete();
    }

    public int getExifOrientation(String imagePath) {
        int degree = 0;
        ExifInterface exif = null;
        try{
            exif = new ExifInterface(imagePath);
        } catch (IOException e){
            e.printStackTrace();
        }
        if(exif != null){
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if(orientation != -1){
                switch(orientation){
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }
        return degree;
    }
}


