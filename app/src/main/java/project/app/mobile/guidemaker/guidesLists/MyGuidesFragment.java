package project.app.mobile.guidemaker.guidesLists;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SessionHandler;
import project.app.mobile.guidemaker.User;
import project.app.mobile.guidemaker.viewGuide.ViewGuideActivity;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyGuidesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyGuidesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyGuidesFragment extends Fragment implements AdapterView.OnItemClickListener{


    private SessionHandler session;
    private User user;

    private LocalSqliteDb localDb;
    private boolean isGuideListCreated = false;
    private ListView guidesList;
    private MyGuidesListAdapter adapter;
    private ArrayList<HashMap> guideDataList;


    private OnFragmentInteractionListener mListener;

    public MyGuidesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyGuidesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyGuidesFragment newInstance(String param1, String param2) {
        MyGuidesFragment fragment = new MyGuidesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localDb = new LocalSqliteDb(getActivity(), "localDatabase.db");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_guides, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        session = new SessionHandler(getContext());
        user = session.getUserDetails();
        if( !isGuideListCreated ) {
            getList();
        }
        guidesList = getView().findViewById(R.id.myGuidesList);
        guidesList.setOnItemClickListener(this);
        createGuidesList();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void createGuidesList(){
        adapter = new MyGuidesListAdapter(guideDataList, getContext(), user.getUsername());
        guidesList.setAdapter(adapter);
    }


    private void getList(){
        guideDataList = localDb.getGuideData(user.getUsername());
        isGuideListCreated = true;
    }
}
