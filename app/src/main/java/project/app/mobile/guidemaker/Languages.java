package project.app.mobile.guidemaker;

import java.util.ArrayList;
import java.util.HashMap;

public class Languages {
    private HashMap<String, String> languageCodes;
    private HashMap<String, String> languageByCode;
    private ArrayList<String> supportedLanguages;

    public Languages(){
        languageCodes = new HashMap<>();
        languageByCode = new HashMap<>();
        supportedLanguages = new ArrayList<>();
        setLanguageByCode();
        setLanguageByCodes();
        setSupportedLanguages();
    }

    private void setLanguageByCode(){
        languageCodes.put("English_UK", "en_UK");
        languageCodes.put("Polski", "pl_PL");
        languageCodes.put("Español", "es_ES");
    }

    private void setLanguageByCodes(){
        languageByCode.put("en_UK", "English_UK");
        languageByCode.put("pl_PL", "Polski");
        languageByCode.put("es_ES", "Español");
    }

    public void setSupportedLanguages(){
        supportedLanguages.add("English_UK");
        supportedLanguages.add("Polski");
        supportedLanguages.add("Español");
    }

    public ArrayList<String> getSupportedLanguages(){
        return supportedLanguages;
    }

    public String getLanguageCode(String language){
        return languageCodes.get(language);
    }

    public String getLanguageByCode(String countryCode){
        return languageByCode.get(countryCode);
    }
}
