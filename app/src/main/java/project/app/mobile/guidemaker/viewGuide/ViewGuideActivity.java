package project.app.mobile.guidemaker.viewGuide;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectLanguageDialogFragment;

public class ViewGuideActivity extends AppCompatActivity implements ViewGuideContract.viewGuideView, SelectLanguageDialogFragment.OnLanguageSelectionListener, SelectLanguageDialogFragment.OnFragmentInteractionListener {

    private TextView tvGuideTitle;
    private ImageView ivPreviewImage;
    private TextView tvTouringTime;
    private TextView tvDescription;
    private Button btnViewGuideAsList;
    private Button btnViewGuideAsGrid;
    private Button btnShowMap;
    private Button btnChooseLanguage;
    private Button btnShowMaps;

    private String ratingTxt;

    private int guideId;
    private String language;
    private ArrayList<String> availableLanguages;
    private ViewGuidePresenterImpl presenter;

    private ViewGroup rootView;

    private LocalSqliteDb localDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroll_view);
        rootView = findViewById(R.id.linear_layout);
        View viewGuide = LayoutInflater.from(this).inflate(R.layout.view_guide, rootView, false);

        localDb = new LocalSqliteDb(this, "localDatabase.db");
        ratingTxt = getResources().getString(R.string.rating);

        tvGuideTitle = viewGuide.findViewById(R.id.viewGuideTitle);
        ivPreviewImage = viewGuide.findViewById(R.id.viewGuideImage);
        tvTouringTime = viewGuide.findViewById(R.id.viewGuideTime);
        tvDescription = viewGuide.findViewById(R.id.viewGuideDescription);

        btnViewGuideAsList = viewGuide.findViewById(R.id.buttonViewGuideViewAsList);
        btnViewGuideAsGrid = viewGuide.findViewById(R.id.buttonViewGuideViewAsGrid);

        btnShowMap = viewGuide.findViewById(R.id.buttonViewGuideShowMap);
        btnChooseLanguage = viewGuide.findViewById(R.id.buttonViewGuideLanguage);
        btnShowMaps = viewGuide.findViewById(R.id.buttonViewGuideShowMaps);


        guideId = getIntent().getExtras().getInt("guideId");
        language = getIntent().getExtras().getString("language");

        presenter = new ViewGuidePresenterImpl(this, getApplicationContext());
        presenter.loadGuideData(guideId, language);

        tvDescription.setMovementMethod(new ScrollingMovementMethod());

        btnViewGuideAsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showObjectsAsList(guideId, language);
            }
        });

        btnViewGuideAsGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.showObjectsAsGrid(guideId, language);
            }
        });

        btnShowMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openMap();
            }
        });


        btnChooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                availableLanguages = localDb.getGuideLanguagesFromLocalDb(guideId);
                Fragment selectLanguageDialogFragment = new SelectLanguageDialogFragment(availableLanguages);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.viewGuideActivity, selectLanguageDialogFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnShowMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.showMaps(guideId);
            }
        });

        rootView.addView(viewGuide);
        showComments();

    }

    private void showComments(){
        ArrayList<HashMap> comments = localDb.getCommentDataFromLocalDb(guideId);
        if(comments.size() > 0) {
            TableLayout commentsTable = new TableLayout(this);
            int commentsCount = comments.size();
            for(int i = 0; i < commentsCount; i++) {
                TableRow usernameRow = new TableRow(this);
                TableRow commentRow = new TableRow(this);
                TextView tvUsername = new TextView(this);
                usernameRow.setBackground(getResources().getDrawable(R.drawable.border));
                usernameRow.setGravity(Gravity.CENTER);
                tvUsername.setText(comments.get(i).get("username").toString());
                usernameRow.addView(tvUsername);
                TextView tvRating = new TextView(this);
                TextView tvComment = new TextView(this);
                tvComment.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                LinearLayout.LayoutParams params = new TableRow.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 0, 10, 0);
                tvRating.setLayoutParams(params);
                tvRating.setText(ratingTxt + ":" + comments.get(i).get("rating"));
                tvComment.setText(comments.get(i).get("comment").toString());
                usernameRow.addView(tvRating);
                commentRow.addView(tvComment);
                commentsTable.addView(usernameRow);
                commentsTable.addView(commentRow);
            }
                commentsTable.setGravity(Gravity.BOTTOM);
            rootView.addView(commentsTable);
        }
    }

    public void setTvGuideTitle(String guideTitle) {
        tvGuideTitle.setText(guideTitle);
    }

    public String getGuideTitle(){
        return tvGuideTitle.getText().toString().trim();
    }

    public void setIvPreviewImage(Bitmap previewImage) {
        ivPreviewImage.setImageBitmap(previewImage);
    }

    public void setTvTouringTime(String touringTime) {
        tvTouringTime.setText(touringTime);
    }

    public void setTvDescription(String description) {
        tvDescription.setText(description);
    }

    public long getGuideId(){
        return  guideId;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onLanguageSelection(String chosenLanguage) {
        Intent intent = getIntent();
        intent.putExtra("guideId", guideId);
        intent.putExtra("language", chosenLanguage);
        finish();
        startActivity(intent);
    }
}
