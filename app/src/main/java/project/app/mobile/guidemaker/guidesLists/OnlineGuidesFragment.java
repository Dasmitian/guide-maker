package project.app.mobile.guidemaker.guidesLists;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import project.app.mobile.guidemaker.Activities.MainActivity;
import project.app.mobile.guidemaker.MySingleton;
import project.app.mobile.guidemaker.R;

import static android.view.Gravity.CENTER;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnlineGuidesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OnlineGuidesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnlineGuidesFragment extends Fragment {

    private TableLayout tableLayout;

    private static final String KEY_GUIDENAME = "guidename";
    private static final String KEY_PUBLIC = "public";
    private String guidename;
    private int isPublic;
    private String data_url = "http://guidemaker.gearhostpreview.com/php/list_guides.php";

    private ProgressDialog pDialog;


    private boolean isGuideListCreated = false;

    private OnFragmentInteractionListener mListener;

    public OnlineGuidesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OnlineGuidesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OnlineGuidesFragment newInstance(String param1, String param2) {
        OnlineGuidesFragment fragment = new OnlineGuidesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_online_guides, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void displayLoader(){
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Getting list.. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void getList(){
        displayLoader();
        JsonArrayRequest jsArrayRequest = new JsonArrayRequest
                (Request.Method.POST, data_url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.dismiss();
                        try {
                            for( int i = 0; i < response.length(); i++ ) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                guidename = jsonObject.getString(KEY_GUIDENAME);
                                isPublic = jsonObject.getInt(KEY_PUBLIC);
                                if(isPublic == 0){

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity()).addToRequestQueue(jsArrayRequest);
        isGuideListCreated = true;
    }
}
