package project.app.mobile.guidemaker.createGuide;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.toolbox.JsonObjectRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.Languages;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.MySingleton;
import project.app.mobile.guidemaker.NavDrawerUser;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectLanguageDialogFragment;
import project.app.mobile.guidemaker.SharedPreferencesClass;
import project.app.mobile.guidemaker.interior.InteriorActivity;
import project.app.mobile.guidemaker.outside.OutsideActivity;

public class CreateGuideActivity extends AppCompatActivity implements CreateGuideContract.CreateGuideView, SelectLanguageDialogFragment.OnLanguageSelectionListener, SelectLanguageDialogFragment.OnFragmentInteractionListener {

    private static int REQUEST_LOAD_IMAGE = 1;
    private static int REQUEST_INTERIOR_GUIDE = 2;
    private int rotation = 0;
    int editedGuideId;
    private String imagePath;
    private String imageName;
    private File imageFile;
    private Uri selectedImage;
    private String guideType;
    private boolean readStoragePermissionGranted;
    private boolean writeStoragePermissionGranted;
    private EditText etTitle;
    private EditText etDescription;
    private TextView tvSelectedLanguage;
    RadioButton radioButtonInterior;
    //RadioButton radioButtonOutside;
    private boolean editing = false;
    private boolean nextButtonClicked = false;

    private ProgressDialog pDialog;
    private CreateGuidePresenterImpl presenter;
    private SharedPreferencesClass preferences;
    private LocalSqliteDb localDb;
    private Languages languages;
    private String language;

    private String titleEmpty;
    private String guideExists;
    private String leaveDialogTitle;
    private String leaveDialogMessage;
    private String saveDialogTitle;
    private String saveDialogMessage;
    private String yesString;
    private String noString;
    private String saving;
    private String chooseLanguage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_guide);
        presenter = new CreateGuidePresenterImpl(this, getApplicationContext());
        etTitle = findViewById(R.id.create_guide_title_text);
        etDescription = findViewById(R.id.create_guide_desc_text);
        preferences = new SharedPreferencesClass(getApplicationContext());
        etTitle.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tvSelectedLanguage = findViewById(R.id.selected_language);

        titleEmpty = getResources().getString(R.string.title_empty);
        guideExists = getResources().getString(R.string.guide_exists);
        leaveDialogTitle = getResources().getString(R.string.leave_dialog_title);
        leaveDialogMessage = getResources().getString(R.string.leave_dialog_message);
        saveDialogTitle = getResources().getString(R.string.save_dialog_title);
        saveDialogMessage = getResources().getString(R.string.save_dalog_message);
        yesString = getResources().getString(R.string.yes);
        noString = getResources().getString(R.string.no);
        saving = getResources().getString(R.string.saving);
        chooseLanguage = getResources().getString(R.string.choose_language);

        localDb = new LocalSqliteDb(this, "localDatabase.db");
        languages = new Languages();

        radioButtonInterior = findViewById(R.id.radioButtonInterior);
        //radioButtonOutside = findViewById(R.id.radioButtonOutside);

        //EDITING MODE

        Intent intent = getIntent();
        Bundle extras;
        extras = intent.getExtras();
        if(extras != null){
            editedGuideId = extras.getInt("guideId");
            editing = extras.getBoolean("editing");
            language = extras.getString("language");
            HashMap guideData = localDb.getGuideFromLocalDb(editedGuideId);
            HashMap guideDataTranslations = localDb.getGuideTranslationsFromLocalDb(editedGuideId, language);
            guideType = String.valueOf(guideData.get("guideType"));
            if(guideType.equals("interior")) {
                HashMap interiorData = localDb.getInteriorFromLocalDb(editedGuideId);
                ArrayList interiorObjects = localDb.getInteriorObjectsFromLocalDb(editedGuideId, language);
                presenter.setInteriorData(interiorData);
                presenter.setInteriorObjectData(interiorObjects, false);
            }
            setGuideData(guideData, guideDataTranslations);
        }

        //EDITING MODE

        Button buttonSelectLanguage = findViewById(R.id.button_language);
        buttonSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectLanguageDialogFragment = new SelectLanguageDialogFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.createGuideActivity, selectLanguageDialogFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button buttonPreviewImage = findViewById(R.id.buttonPreviewImage);
        buttonPreviewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_IMAGE);
            }
        });


        guideType = "interior";

        Button buttonSave = findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
                if (writeStoragePermissionGranted) {
                    save();
                }
            }
        });

        Button buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
                if (writeStoragePermissionGranted) {
                    initProgressDialog();
                    preferences.setGuideName(getEtTitle());
                    switch (presenter.handleNextButton(getEtTitle(), editedGuideId, nextButtonClicked, editing, getTvSelectedLanguage())){
                        case 0:
                            nextButtonClicked = true;
                            loadInterior();
                            break;
                        case 1:
                            showToastMessage(titleEmpty);
                            pDialog.dismiss();
                            break;
                        case 2:
                            showToastMessage(guideExists);
                            pDialog.dismiss();
                            break;
                        case 3:
                            showToastMessage(chooseLanguage);
                            pDialog.dismiss();
                            break;
                    }
                }
            }
        });

        etTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    presenter.checkIfGuideExists(getEtTitle());
                }
            }
        });

        etTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( actionId == EditorInfo.IME_ACTION_DONE ){
                    closeKeyboard();
                    presenter.checkIfGuideExists(getEtTitle());
                    return true;
                }
                return false;
            }
        });
    }



    private void closeKeyboard(){
        View view = this.getCurrentFocus();
        if(view != null){
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void setEtTitleError(String message) {
        etTitle.setError(message);
        etTitle.requestFocus();
    }

    public void setGuideData(HashMap guideData, HashMap guideDataTranslations){
        if( guideType.equals("interior")) {
            radioButtonInterior.toggle();
        } else {
            //radioButtonOutside.toggle();
        }
        etTitle.setText(String.valueOf(guideDataTranslations.get("guideTitle")));
        etDescription.setText(String.valueOf(guideDataTranslations.get("guideDescription")));
        setTvSelectedLanguage(language);
        imageName = String.valueOf(guideData.get("previewImageName"));
        imagePath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + getEtTitle() + "/" + imageName;
        imageFile = new File(imagePath);
        loadImage();
    }

    private void save(){
        initProgressDialog();
        if(!presenter.isTitleEmpty(getEtTitle())) {
            switch( presenter.handleSaveButton(getEtTitle(), imageFile, editing, editedGuideId)){
                case 0: showToastMessage(getResources().getString(R.string.saved));
                    break;
                case 1: showToastMessage(getResources().getString(R.string.not_saved_exists));
            }
        } else {
            showToastMessage(getResources().getString(R.string.not_saved_empty));
        }
        pDialog.dismiss();
    }

    @Override
    public String getTvSelectedLanguage(){
        return tvSelectedLanguage.getText().toString();
    }

    private void setTvSelectedLanguage(String language){
        tvSelectedLanguage.setText(language);
    }

    @Override
    public String getEtTitle(){
        return etTitle.getText().toString().trim();
    }

    @Override
    public String getImageName(){
        return imageName;
    }

    @Override
    public String getEtDescription(){
        return etDescription.getText().toString().trim();
    }

    @Override
    public String getGuideType(){
        return guideType;
    }

    @Override
    public void onBackPressed(){
        backButtonHandler();
    }

    private void backButtonHandler(){
        AlertDialog.Builder alertDialogLeaving = new AlertDialog.Builder(CreateGuideActivity.this);
        alertDialogLeaving.setTitle(leaveDialogTitle);
        alertDialogLeaving.setMessage(leaveDialogMessage);
        alertDialogLeaving.setPositiveButton(yesString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!presenter.checkSaveStatus()){
                    AlertDialog.Builder alertDialogSaving = new AlertDialog.Builder(CreateGuideActivity.this);
                    alertDialogSaving.setTitle(saveDialogTitle);
                    alertDialogSaving.setMessage(saveDialogMessage);
                    alertDialogSaving.setPositiveButton(yesString, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            save();
                            loadNavDrawerUser();
                        }
                    });
                    alertDialogSaving.setNegativeButton(noString, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(!editing) {
                                presenter.deleteGuideFolder(getEtTitle());
                            }
                            initProgressDialog();
                            loadNavDrawerUser();
                        }
                    });
                    alertDialogSaving.show();
                } else {
                    initProgressDialog();
                    loadNavDrawerUser();
                }
            }
        });

        alertDialogLeaving.setNegativeButton(noString, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialogLeaving.show();
    }

    public void loadOutside(){
        pDialog.hide();
        pDialog.dismiss();
        Intent i = new Intent(getApplicationContext(), OutsideActivity.class);
        startActivity(i);
    }

    public void loadInterior(){
        pDialog.hide();
        pDialog.dismiss();
        if( imageFile == null ){
            imageName = "";
        }
        Intent i = new Intent(getApplicationContext(), InteriorActivity.class);
        i.putExtra("interiorData", presenter.getInteriorData());
        i.putExtra("interiorObjectsData", presenter.getInteriorObjectData());
        if(nextButtonClicked){
            i.putExtra("editing", true);
        } else {
            i.putExtra("editing", editing);
        }
        i.putExtra("language", getTvSelectedLanguage());
        startActivityForResult(i, REQUEST_INTERIOR_GUIDE);
    }

    public void loadNavDrawerUser(){
        pDialog.hide();
        pDialog.dismiss();
        Intent i = new Intent(getApplicationContext(), NavDrawerUser.class);
        startActivity(i);
        finish();
    }

    public void initProgressDialog(){
        pDialog = new ProgressDialog(CreateGuideActivity.this);
        pDialog.setMessage(saving);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected void onPause(){
        super.onPause();
        requestPermission();
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(readStoragePermissionGranted){
            if( requestCode == REQUEST_LOAD_IMAGE && resultCode == RESULT_OK && null != data ){
                selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToLast();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imageFile = new File(picturePath);
                imagePath = imageFile.getAbsolutePath();
                imageName = imageFile.getName();
                loadImage();
                cursor.close();
            } else if ( requestCode == REQUEST_INTERIOR_GUIDE && resultCode == RESULT_OK && data != null ) {
                presenter.setInteriorData((HashMap) data.getSerializableExtra("interiorData"));
                presenter.setInteriorObjectData((ArrayList<HashMap>) data.getSerializableExtra("objectsArray"), data.getBooleanExtra("itemsSwapped", false));
            }
        }
    }

    private void loadImage(){
        ImageView imageView = findViewById(R.id.previewImageView);
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        rotation = presenter.getExifOrientation(imageFile.getAbsolutePath());
        imageView.setImageBitmap(presenter.loadImage(rotation, bitmap));
    }

    private void requestPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(CreateGuideActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(CreateGuideActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

        } else {
            readStoragePermissionGranted = true;
        }

        if( ContextCompat.checkSelfPermission(CreateGuideActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions( CreateGuideActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            writeStoragePermissionGranted = true;
        }
    }

    @Override
    public boolean checkPermission(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) { readStoragePermissionGranted = true;
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void showToastMessage(String message){
        Toast.makeText(getApplicationContext(), message , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void accessSingleton(JsonObjectRequest jsObjectRequest){
        MySingleton.getInstance(this).addToRequestQueue(jsObjectRequest);
    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.radioButtonInterior:
                if(checked){
                    guideType = "interior";
                }
                break;
        }
    }

    @Override
    public void onLanguageSelection(String chosenLanguage) {
        tvSelectedLanguage.setText(chosenLanguage);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
