package project.app.mobile.guidemaker.translate;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.Languages;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;

public class TranslateGuideActivity extends AppCompatActivity implements TranslateObjectFragment.OnFragmentInteractionListener, TranslateGuideFragment.OnFragmentInteractionListener{

    private ArrayList<HashMap<String, String>> interiorObjectsDataList;
    private int guideId;
    private LocalSqliteDb localDb;
    private HashMap guideTranslationsData;
    private String translateFrom;
    private String translateTo;
    private Languages languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate_guide);

        Bundle extras = getIntent().getExtras();
        String guideTitle = extras.getString("guideTitle");
        guideId = extras.getInt("guideId");
        translateFrom = extras.getString("translateFrom");
        translateTo = extras.getString("translateTo");
        localDb = new LocalSqliteDb(this, "localDatabase.db");
        languages = new Languages();
        getInteriorObjectsDataList();
        getGuideTranslationsData();

        TabLayout tabLayout = findViewById(R.id.translateTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(guideTranslationsData.get("guideTitle").toString()));
        int listSize = interiorObjectsDataList.size();
        for (int i = 0; i < listSize; i++) {
            String tabName = interiorObjectsDataList.get(i).get("objectName");
            tabLayout.addTab(tabLayout.newTab().setText(tabName));
        }
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.viewPagerTranslate);
        final TranslateViewPagerAdapter translateViewPagerAdapter = new TranslateViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), interiorObjectsDataList, guideTranslationsData, guideTitle, translateTo, guideId);
        viewPager.setAdapter(translateViewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getInteriorObjectsDataList(){
        interiorObjectsDataList = localDb.getInteriorObjectsFromLocalDb(guideId, translateFrom);
    }

    private void getGuideTranslationsData(){
        guideTranslationsData = localDb.getGuideTranslationsFromLocalDb(guideId, translateFrom);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
