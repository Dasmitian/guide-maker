package project.app.mobile.guidemaker.guidesLists;

import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectLanguageDialogFragment;
import project.app.mobile.guidemaker.SessionHandler;

public class ListGuides extends AppCompatActivity implements OnlineGuidesFragment.OnFragmentInteractionListener, DownloadedGuidesFragment.OnFragmentInteractionListener,
        MyGuidesFragment.OnFragmentInteractionListener, TranslateLanguagesListDialogFragment.OnFragmentInteractionListener, SelectLanguageDialogFragment.OnFragmentInteractionListener, GuideLanguageToEditFragment.OnFragmentInteractionListener {

    private SessionHandler session;

    private String onlineGuides;
    private String downloadedGuides;
    private String myGuides;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_guides);

        session = new SessionHandler(this);

        onlineGuides = getResources().getString(R.string.online_guides);
        downloadedGuides = getResources().getString(R.string.downloaded_guides);
        myGuides = getResources().getString(R.string.my_guides);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        //tabLayout.addTab(tabLayout.newTab().setText(onlineGuides));
        tabLayout.addTab(tabLayout.newTab().setText(downloadedGuides));
        if( session.isLoggedIn()) {
            tabLayout.addTab(tabLayout.newTab().setText(myGuides));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.viewPager);
        final PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
