package project.app.mobile.guidemaker.interior;

import android.Manifest;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.MySingleton;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectMapDialogFragment;
import project.app.mobile.guidemaker.SharedPreferencesClass;

public class InteriorActivity extends AppCompatActivity implements InteriorContract.InteriorView, AddPoiFragment.OnObjectDataPass, SelectedMapsFragment.OnFragmentInteractionListener, SelectMapDialogFragment.OnFragmentInteractionListener {

    private Fragment fragment;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private InteriorPresenterImpl presenter;
    private SharedPreferencesClass preferences;
    private InteriorListAdapter adapter;

    private TextView tvLocation;
    private EditText etTouringTime;

    private static int REQUEST_LOAD_IMAGE = 1;
    private File mapFile;
    private boolean mapButtonClicked = false;
    private ListView objectsList;
    private String oldMapFileName;
    private boolean editing;
    private String language;

    private ArrayList<String> mapsNamesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interior);

        final Button locationButton = findViewById(R.id.locationButton);
        Button mapButton = findViewById(R.id.mapButton);
        Button newObjectButton = findViewById(R.id.newObjectButton);
        Button saveButton = findViewById(R.id.saveButton);
        Button showMapsButton = findViewById(R.id.addedMapsButton);
        tvLocation = findViewById(R.id.selectedLocationLabel);
        etTouringTime = findViewById(R.id.etTouringTime);
        objectsList = findViewById(R.id.objectsList);

        presenter = new InteriorPresenterImpl(this, getApplicationContext());
        preferences = new SharedPreferencesClass(getApplicationContext());
        mapsNamesList = new ArrayList<>();

        Intent intent = getIntent();
        Bundle extras;
        extras = intent.getExtras();
        if( extras != null) {
            editing = extras.getBoolean("editing");
            language = extras.getString("language");
            if( editing ){
                presenter.loadInteriorData((HashMap) extras.getSerializable("interiorData"));
                presenter.loadInteriorObjectsData((ArrayList<HashMap<String, String>>) extras.getSerializable("interiorObjectsData"));
            }
        }
        createObjectsList();

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new InteriorMapFragment();
                fragmentManager.beginTransaction().replace(R.id.interior_container, fragment).addToBackStack(null).commit();
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_IMAGE);
            }
        });

        showMapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectedMaps();
            }
        });

        newObjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPoiFragment();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( validateInputs() ) {
                    setResult(RESULT_OK, presenter.handleSaveButton());
                    finish();
                }
            }
        });
    }

    private void showSelectedMaps(){
        Fragment selectedMapsFragment = new SelectedMapsFragment().newInstance(mapsNamesList, preferences.getGuideName());
        fragmentManager.beginTransaction().replace(R.id.interior_container, selectedMapsFragment).addToBackStack(null).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.current_place_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed(){
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            createObjectsList();
        }
    }

    @Override
    public void startPoiFragment(){
        fragment = new AddPoiFragment().newInstance(false, -1,null, language, mapsNamesList);
        fragmentManager.beginTransaction().replace(R.id.interior_container, fragment, "poiFragment").addToBackStack("addPoi").commit();
    }

    @Override
    public void editPoiFragment(int position){
        HashMap objectData;
        objectData = presenter.getObjectDataFromList(position);
        fragment = new AddPoiFragment().newInstance(true, position, objectData, language, mapsNamesList);
        fragmentManager.beginTransaction().replace(R.id.interior_container, fragment, "poiFragment").addToBackStack("addPoi").commit();
    }

    public void swapItemsInArray(int item1, int item2){
        presenter.swapItemsInArray(item1, item2);
    }

    @Override
    public void deleteObjectFromArray(int position){
        presenter.deleteObjectFromArray(position);
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == REQUEST_LOAD_IMAGE && resultCode == RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            mapFile = new File(picturePath);
            mapsNamesList.add(mapFile.getName());
            presenter.copyMapFile(mapFile);
        }
    }

    @Override
    public void createObjectsList(){
        adapter = new InteriorListAdapter(presenter.getObjectNameFromList(), this);
        objectsList.setAdapter(adapter);
    }

    @Override
    public void setTvLocation(String location){
        tvLocation.setText(location);
    }

    @Override
    public String getTvLocation(){
        return tvLocation.getText().toString().trim();
    }

    @Override
    public void setEtTouringTime(String touringTime){
        etTouringTime.setText(touringTime);
    }

    @Override
    public String getEtTouringTime(){
        return etTouringTime.getText().toString().trim();
    }

    @Override
    public ArrayList<String> getMapsNamesList(){
        return mapsNamesList;
    }

    @Override
    public void setMapsNamesList(ArrayList<String> mapsNames){
        mapsNamesList = mapsNames;
    }

    @Override
    public void setPoiInfo(String poiName, String poiId, double poiLongitude, double poiLatitude){
        tvLocation.setError(null);
        tvLocation.setText(poiName);
        presenter.passPoiInfo(poiName, poiId, poiLongitude, poiLatitude);
    }

    @Override
    public boolean checkReadStoragePermission(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode){
            case 1:
                if(grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                }
        }
    }

    @Override
    public void showToastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean validateInputs(){
        if( getTvLocation().equals("") ){
            tvLocation.requestFocus();
            tvLocation.setError("Choose location!");
            return false;
        }
        return true;
    }

    @Override
    public void accessSingleton(JsonObjectRequest jsObjectRequest){
        MySingleton.getInstance(this).addToRequestQueue(jsObjectRequest);
    }


    @Override
    public void passObjectData(HashMap objectData, int position) {
        if( position >= 0 ){
            presenter.editObjectInArray(position, objectData);
        } else {
            presenter.addObjectToList(objectData);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
