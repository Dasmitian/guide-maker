package project.app.mobile.guidemaker.viewGuide;

import android.database.Cursor;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;

public interface ViewGuideContract {

    interface viewGuideView{
        void setTvGuideTitle(String guideTitle);
        void setIvPreviewImage(Bitmap previewImage);
        void setTvTouringTime(String touringTime);
        void setTvDescription(String description);
        long getGuideId();
        String getGuideTitle();
    }

    interface viewGuidePresenter{

    }

    interface  viewGuideModel{
        HashMap getGuideFromDb(int guideId);
        HashMap getGuideTranslationsFromDb(int guideId, String language);
        HashMap getInteriorFromDb(int guideId);
        ArrayList getInteriorObjectsFromDb(int guideId, String language);
        ArrayList getInteriorMaps(int guideId);
    }
}
