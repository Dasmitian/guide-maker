package project.app.mobile.guidemaker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class LocalSqliteDb extends SQLiteOpenHelper {

    private String databaseName;

    private static final String OBJECTS_TABLE = "interior_object_translations";
    private static final String OBJECTS_COLUMN_ID = "interior_object_id";
    private static final String OBJECTS_COLUMN_NAME = "object_name";
    private static final String OBJECTS_COLUMN_AUDIO_FILE = "audio_file_name";
    private static final String OBJECTS_COLUMN_IMAGE_FILE = "object_image_name";
    private static final String OBJECTS_COLUMN_DESCRIPTION = "object_description";
    private static final String OBJECTS_COLUMN_GUIDE_ID = "guide_id";
    private static final String OBJECTS_COLUMN_LANGUAGE = "language";
    private static final String OBJECTS_COLUMN_LIST_POSITION = "list_position";
    private static final String OBJECTS_COLUMN_PIN_X = "pin_pos_x";
    private static final String OBJECTS_COLUMN_PIN_Y = "pin_pos_y";
    private static final String OBJECTS_COLUMN_MAP_NAME = "map_file_name";

    private static final String GUIDES_TABLE = "guides";
    private static final String GUIDES_COLUMNN_ID = "guide_id";
    private static final String GUIDES_COLUMN_USERNAME = "username";
    private static final String GUIDES_COLUMN_IMAGE_FILE = "preview_image_name";
    private static final String GUIDES_COLUMN_TYPE = "guide_type";
    private static final String GUIDES_COLUMN_CREATION_TIME = "creation_timestamp";
    private static final String GUIDES_COLUMN_UPDATE_TIME = "update_timestamp";

    private static final String GUIDES_TRANSLATIONS_TABLE = "guides_translations";
    private static final String GUIDES_TRANSLATIONS_ID = "guide_id";
    private static final String GUIDES_TRANSLATIONS_TITLE = "guide_title";
    private static final String GUIDES_TRANSLATIONS_DESCRIPTION = "description";
    private static final String GUIDES_TRANSLATIONS_LANGUAGE = "language";
    private static final String GUIDES_TRANSLATIONS_IS_DEFAULT = "is_default";
    private static final String GUIDES_TRANSLATIONS_ORIGINAL_TITLE = "original_guide_title";

    private static final String INTERIOR_TABLE = "interior";
    private static final String INTERIOR_COLUMN_ID = "interior_guide_id";
    private static final String INTERIOR_COLUMN_TIME = "touring_time";
    private static final String INTERIOR_COLUMN_POI_ID = "poi_id";
    private static final String INTERIOR_COLUMN_POI_NAME = "poi_name";
    private static final String INTERIOR_COLUMN_POI_LAT = "poi_lat";
    private static final String INTERIOR_COLUMN_POI_LONG = "poi_long";
    private static final String INTERIOR_COLUMN_MAPS_NAMES = "maps_names_list";
    private static final String INTERIOR_COLUMN_GUIDE_ID = "guide_id";

    private static final String COMMENT_TABLE = "comments";
    private static final String COMMENT_COLUMN_TEXT = "comment_text";
    private static final String COMMENT_COLUMN_RATING = "rating";
    private static final String COMMENT_COLUMN_USERNAME = "username";
    private static final String COMMENT_COLUMN_GUIDE_ID = "guide_id";

    public LocalSqliteDb(Context context, String dbName) {
        super(context, dbName, null, 1);
        this.databaseName = dbName;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (databaseName == "localDatabase.db") {
            db.execSQL("create table if not exists guides (guide_id integer primary key, username text, preview_image_name text, guide_type text, creation_timestamp timestamp, update_timestamp timestamp)");
            db.execSQL("create table if not exists interior (interior_guide_id integer primary key autoincrement, touring_time text, poi_id text, poi_name text, poi_lat double, poi_long double, maps_names_list text, guide_id integer, foreign key (guide_id) references guides(guide_id))");
            //FOR TESTING, MOVE TO SERVER LATER
            db.execSQL("create table if not exists comments (comment_id integer primary key autoincrement, rating integer, comment_text text, username text, guide_id integer, foreign key (guide_id) references guides(guide_id))");

            db.execSQL("create table if not exists guides_translations (guide_id integer, language text, is_default integer, guide_title text, description text, original_guide_title text, foreign key (guide_id) references guides(guide_id))");
            db.execSQL("create table if not exists interior_object_translations (interior_object_id integer primary key autoincrement, list_position integer, guide_id integer, language text, is_default integer, object_name text, audio_file_name text, object_image_name text, object_description text, pin_pos_x text, pin_pos_y text, map_file_name text, foreign key (guide_id) references guides(guide_id))");


            /*
            db.execSQL("create table if not exists guides (guide_id integer primary key autoincrement, username text, guide_title text unique, description text, preview_image_name text, guide_type text, creation_timestamp timestamp, update_timestamp timestamp)");
            db.execSQL("create table if not exists interior (interior_guide_id integer primary key autoincrement, touring_time text, poi_id text, poi_name text, poi_lat double, poi_long double, map_file_name text, guide_id integer, foreign key (guide_id) references guides(guide_id))");
            db.execSQL("create table if not exists interior_objects (interior_object_id integer primary key autoincrement, object_name text, audio_file_name text, object_image_name text, object_description text, guide_id integer, foreign key (guide_id) references guides(guide_id))");
            //FOR TESTING, MOVE TO SERVER LATER
            db.execSQL("create table if not exists comments (comment_id integer primary key autoincrement, rating integer, comment_text text, username text, guide_id integer, foreign key (guide_id) references guides(guide_id))");
            db.execSQL("create table if not exists guides_translations (guide_id integer, language text, is_default integer, title text, description text, foreign key (guide_id) references guides(guide_id))");
            db.execSQL("create table if not exists interior_object_translations (guide_id integer, language text, is_default integer, audio_file_name text, object_description text, foreign key (guide_id) references guides(guide_id))");
             */

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS guides");
        db.execSQL("DROP TABLE IF EXISTS interior");
        db.execSQL("DROP TABLE IF EXISTS interior_objects");
        db.execSQL("DROP TABLE IF EXISTS comments");
        db.execSQL("DROP TABLE IF EXISTS guides_translations");
        db.execSQL("DROP TABLE IF EXISTS interior_object_translations");
        onCreate(db);
    }

    public void dropLocalDbTables() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS guides");
        db.execSQL("DROP TABLE IF EXISTS interior");
        db.execSQL("DROP TABLE IF EXISTS interior_objects");
        db.execSQL("DROP TABLE IF EXISTS comments");
        db.execSQL("DROP TABLE IF EXISTS guides_translations");
        db.execSQL("DROP TABLE IF EXISTS interior_object_translations");
    }

    public void createLocalDbTables() {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
    }

    public int generateId(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT guide_id FROM guides ORDER BY guide_id", null);
        if(cursor.getCount() <= 0){
            return 0;
        } else {
            cursor.moveToLast();
            return cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID)) + 1;
        }
    }

    public void insertCommentToLocalDb(String comment, int rating, String username, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COMMENT_COLUMN_RATING, rating);
        contentValues.put(COMMENT_COLUMN_TEXT, comment);
        contentValues.put(COMMENT_COLUMN_USERNAME, username);
        contentValues.put(COMMENT_COLUMN_GUIDE_ID, guideId);
        db.insert(COMMENT_TABLE, null, contentValues);
    }

    public ArrayList<HashMap> getCommentDataFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap commentData;
        ArrayList commentDataList = new ArrayList();
        Cursor cursor = db.rawQuery("SELECT * FROM comments WHERE " + COMMENT_COLUMN_GUIDE_ID + " = " + "'" + guideId + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()) {
                commentData = new HashMap();
                commentData.put("comment", cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN_TEXT)));
                commentData.put("rating", cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN_RATING)));
                commentData.put("username", cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN_USERNAME)));
                commentDataList.add(commentData);
            }
            cursor.close();
            return commentDataList;
        }
        return null;
    }

    public HashMap getUserCommentFromLocalDb(String username, int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap commentData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT " + COMMENT_COLUMN_TEXT + "," + COMMENT_COLUMN_RATING + " FROM " + COMMENT_TABLE + " WHERE " + COMMENT_COLUMN_USERNAME + " = " + "'" + username + "'" + " AND " + COMMENT_COLUMN_GUIDE_ID + " = " + "'" + guideId + "'", null);
        if(cursor == null){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                commentData.put("comment", cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN_TEXT)));
                commentData.put("rating", cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN_RATING)));
                cursor.close();
                return commentData;
            }
        }
        return null;
    }

    public void updateUserCommentInLocalDb(int rating, String comment, int guideId, String username){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COMMENT_COLUMN_RATING, rating);
        contentValues.put(COMMENT_COLUMN_TEXT, comment);
        db.update(COMMENT_TABLE, contentValues, "guide_id = ? AND username = ?", new String[]{String.valueOf(guideId), username});
        contentValues.clear();
    }

    public void insertGuideTranslationToLocalDb(HashMap guideTranslations, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Cursor cursor = db.rawQuery("SELECT guide_id FROM guides_translations WHERE guide_id = " + "'" + guideId + "'", null);
        if(!cursor.moveToFirst()){
            contentValues.put(GUIDES_TRANSLATIONS_IS_DEFAULT, true);
        }
        cursor.close();
        contentValues.put(GUIDES_TRANSLATIONS_ID, guideId);
        contentValues.put(GUIDES_TRANSLATIONS_ORIGINAL_TITLE, String.valueOf(guideTranslations.get("originalGuideTitle")));
        contentValues.put(GUIDES_TRANSLATIONS_LANGUAGE, String.valueOf(guideTranslations.get("language")));
        contentValues.put(GUIDES_TRANSLATIONS_TITLE, String.valueOf(guideTranslations.get("guideTitle")));
        contentValues.put(GUIDES_TRANSLATIONS_DESCRIPTION, String.valueOf(guideTranslations.get("guideDescription")));
        db.insert(GUIDES_TRANSLATIONS_TABLE, null, contentValues);
        contentValues.clear();
    }



    public int insertGuideToLocalDb(HashMap guideData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String creationTimestamp = dateFormat.format(new Date());
        int guideId = generateId();
        contentValues.put(GUIDES_COLUMNN_ID, guideId);
        contentValues.put(GUIDES_COLUMN_USERNAME, String.valueOf(guideData.get("username")));
        contentValues.put(GUIDES_COLUMN_IMAGE_FILE, String.valueOf(guideData.get("imageName")));
        contentValues.put(GUIDES_COLUMN_TYPE, String.valueOf(guideData.get("guideType")));
        contentValues.put(GUIDES_COLUMN_CREATION_TIME, creationTimestamp);
        db.insert(GUIDES_TABLE, null, contentValues);
        contentValues.clear();
        return guideId;
    }

    public void insertInteriorToLocalDb(HashMap interiorData, int guideId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String> mapsNamesList = (ArrayList<String>) interiorData.get("mapsNamesList");
        Gson gson = new Gson();
        String mapsNamesString = gson.toJson(mapsNamesList);
        ContentValues contentValues = new ContentValues();
        contentValues.put(INTERIOR_COLUMN_TIME, String.valueOf(interiorData.get("touringTime")));
        contentValues.put(INTERIOR_COLUMN_POI_ID, String.valueOf(interiorData.get("poiId")));
        contentValues.put(INTERIOR_COLUMN_POI_NAME, String.valueOf(interiorData.get("poiName")));
        contentValues.put(INTERIOR_COLUMN_POI_LAT, Double.parseDouble(String.valueOf(interiorData.get("poiLat"))));
        contentValues.put(INTERIOR_COLUMN_POI_LONG, Double.parseDouble(String.valueOf(interiorData.get("poiLong"))));
        contentValues.put(INTERIOR_COLUMN_MAPS_NAMES, mapsNamesString);
        contentValues.put(INTERIOR_COLUMN_GUIDE_ID, guideId);
        db.insert(INTERIOR_TABLE, null, contentValues);
        contentValues.clear();
    }

    public void insertInteriorObjectsTranslationsToLocalDb(ArrayList<HashMap> interiorObjectsData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        int listSize = interiorObjectsData.size();
        for( int i = 0; i < listSize; i++){
            contentValues.put(OBJECTS_COLUMN_NAME, String.valueOf(interiorObjectsData.get(i).get("objectName")));
            contentValues.put(OBJECTS_COLUMN_AUDIO_FILE, String.valueOf(interiorObjectsData.get(i).get("audioFileName")));
            contentValues.put(OBJECTS_COLUMN_IMAGE_FILE, String.valueOf(interiorObjectsData.get(i).get("imageFileName")));
            contentValues.put(OBJECTS_COLUMN_DESCRIPTION, String.valueOf(interiorObjectsData.get(i).get("objectDescription")));
            contentValues.put(OBJECTS_COLUMN_LANGUAGE, String.valueOf(interiorObjectsData.get(i).get("language")));
            contentValues.put(OBJECTS_COLUMN_LIST_POSITION, String.valueOf(interiorObjectsData.get(i).get("listPosition")));
            contentValues.put(OBJECTS_COLUMN_PIN_X, String.valueOf(interiorObjectsData.get(i).get("pinPosX")));
            contentValues.put(OBJECTS_COLUMN_PIN_Y, String.valueOf(interiorObjectsData.get(i).get("pinPosY")));
            contentValues.put(OBJECTS_COLUMN_MAP_NAME, String.valueOf(interiorObjectsData.get(i).get("mapName")));
            contentValues.put(OBJECTS_COLUMN_GUIDE_ID, guideId);
            db.insert(OBJECTS_TABLE, null, contentValues);
            contentValues.clear();
        }
    }

    public void insertInteriorObjectTranslationsToLocalDb(HashMap interiorObjectData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OBJECTS_COLUMN_NAME, String.valueOf(interiorObjectData.get("objectName")));
        contentValues.put(OBJECTS_COLUMN_AUDIO_FILE, String.valueOf(interiorObjectData.get("audioFileName")));
        contentValues.put(OBJECTS_COLUMN_IMAGE_FILE, String.valueOf(interiorObjectData.get("imageFileName")));
        contentValues.put(OBJECTS_COLUMN_DESCRIPTION, String.valueOf(interiorObjectData.get("objectDescription")));
        contentValues.put(OBJECTS_COLUMN_LANGUAGE, String.valueOf(interiorObjectData.get("language")));
        contentValues.put(OBJECTS_COLUMN_LIST_POSITION, String.valueOf(interiorObjectData.get("listPosition")));
        contentValues.put(OBJECTS_COLUMN_PIN_X, String.valueOf(interiorObjectData.get("pinPosX")));
        contentValues.put(OBJECTS_COLUMN_PIN_Y, String.valueOf(interiorObjectData.get("pinPosY")));
        contentValues.put(OBJECTS_COLUMN_MAP_NAME, String.valueOf(interiorObjectData.get("mapName")));
        contentValues.put(OBJECTS_COLUMN_GUIDE_ID, guideId);
        db.insert(OBJECTS_TABLE, null, contentValues);
        contentValues.clear();
    }

    public void updateGuideInLocalDb(HashMap guideData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String updateTimestamp = dateFormat.format(new Date());
        contentValues.put(GUIDES_COLUMN_USERNAME, String.valueOf(guideData.get("username")));
        contentValues.put(GUIDES_COLUMN_IMAGE_FILE, String.valueOf(guideData.get("imageName")));
        contentValues.put(GUIDES_COLUMN_TYPE, String.valueOf(guideData.get("guideType")));
        contentValues.put(GUIDES_COLUMN_UPDATE_TIME, updateTimestamp);
        db.update(GUIDES_TABLE, contentValues, "guide_id = ?", new String[]{String.valueOf(guideId)});
    }

    public void updateGuideTranslationsInLocalDb(HashMap guideTranslationsData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GUIDES_TRANSLATIONS_LANGUAGE, String.valueOf(guideTranslationsData.get("language")));
        //contentValues.put(GUIDES_TRANSLATIONS_IS_DEFAULT, String.valueOf(guideTranslationsData.get("imageName")));
        contentValues.put(GUIDES_TRANSLATIONS_ORIGINAL_TITLE, String.valueOf(guideTranslationsData.get("originalGuideTitle")));
        contentValues.put(GUIDES_TRANSLATIONS_TITLE, String.valueOf(guideTranslationsData.get("guideTitle")));
        contentValues.put(GUIDES_TRANSLATIONS_DESCRIPTION, String.valueOf(guideTranslationsData.get("guideDescription")));
        db.update(GUIDES_TRANSLATIONS_TABLE, contentValues, "guide_id = ?", new String[]{String.valueOf(guideId)});
    }

    public void updateInteriorInLocalDb(HashMap interiorData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String> mapsNamesList = (ArrayList<String>) interiorData.get("mapsNamesList");
        Gson gson = new Gson();
        String mapsNamesString = gson.toJson(mapsNamesList);
        ContentValues contentValues = new ContentValues();
        contentValues.put(INTERIOR_COLUMN_TIME, String.valueOf(interiorData.get("touringTime")));
        contentValues.put(INTERIOR_COLUMN_POI_ID, String.valueOf(interiorData.get("poiId")));
        contentValues.put(INTERIOR_COLUMN_POI_NAME, String.valueOf(interiorData.get("poiName")));
        contentValues.put(INTERIOR_COLUMN_POI_LAT, Double.parseDouble(String.valueOf(interiorData.get("poiLat"))));
        contentValues.put(INTERIOR_COLUMN_POI_LONG, Double.parseDouble(String.valueOf(interiorData.get("poiLong"))));
        contentValues.put(INTERIOR_COLUMN_MAPS_NAMES, mapsNamesString);
        db.update(INTERIOR_TABLE, contentValues, "guide_id = ?", new String[]{String.valueOf(guideId)});
    }

    public void updateInteriorObjectPosition(HashMap interiorObjectData){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OBJECTS_COLUMN_LIST_POSITION, String.valueOf(interiorObjectData.get("listPosition")));
        db.update(OBJECTS_TABLE, contentValues, "interior_object_id = ?", new String[]{String.valueOf(interiorObjectData.get("objectId"))});
    }

    public void updateInteriorObjectsInLocalDb(ArrayList<HashMap> interiorObjectsData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        int listSize = interiorObjectsData.size();
        for( int i = 0; i < listSize; i++){
            contentValues.put(OBJECTS_COLUMN_NAME, String.valueOf(interiorObjectsData.get(i).get("objectName")));
            contentValues.put(OBJECTS_COLUMN_AUDIO_FILE, String.valueOf(interiorObjectsData.get(i).get("audioFileName")));
            contentValues.put(OBJECTS_COLUMN_IMAGE_FILE, String.valueOf(interiorObjectsData.get(i).get("imageFileName")));
            contentValues.put(OBJECTS_COLUMN_DESCRIPTION, String.valueOf(interiorObjectsData.get(i).get("objectDescription")));
            contentValues.put(OBJECTS_COLUMN_LIST_POSITION, String.valueOf(interiorObjectsData.get(i).get("listPosition")));
            contentValues.put(OBJECTS_COLUMN_PIN_X, String.valueOf(interiorObjectsData.get(i).get("pinPosX")));
            contentValues.put(OBJECTS_COLUMN_PIN_Y, String.valueOf(interiorObjectsData.get(i).get("pinPosY")));
            contentValues.put(OBJECTS_COLUMN_MAP_NAME, String.valueOf(interiorObjectsData.get(i).get("mapName")));
            db.update(OBJECTS_TABLE, contentValues, "guide_id = ? AND interior_object_id = ?", new String[]{String.valueOf(guideId), String.valueOf(interiorObjectsData.get(i).get("objectId"))});
            contentValues.clear();
        }
    }

    public void updateInteriorObjectInLocalDb(HashMap interiorObjectData, int guideId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OBJECTS_COLUMN_NAME, String.valueOf(interiorObjectData.get("objectName")));
        contentValues.put(OBJECTS_COLUMN_AUDIO_FILE, String.valueOf(interiorObjectData.get("audioFileName")));
        contentValues.put(OBJECTS_COLUMN_IMAGE_FILE, String.valueOf(interiorObjectData.get("imageFileName")));
        contentValues.put(OBJECTS_COLUMN_DESCRIPTION, String.valueOf(interiorObjectData.get("objectDescription")));
        contentValues.put(OBJECTS_COLUMN_LIST_POSITION, String.valueOf(interiorObjectData.get("listPosition")));
        contentValues.put(OBJECTS_COLUMN_PIN_X, String.valueOf(interiorObjectData.get("pinPosX")));
        contentValues.put(OBJECTS_COLUMN_PIN_Y, String.valueOf(interiorObjectData.get("pinPosY")));
        contentValues.put(OBJECTS_COLUMN_MAP_NAME, String.valueOf(interiorObjectData.get("mapName")));
        db.update(OBJECTS_TABLE, contentValues, "guide_id = ? AND interior_object_id = ?", new String[]{String.valueOf(guideId), String.valueOf(interiorObjectData.get("objectId"))});
        contentValues.clear();
    }

    public void manageInteriorObjectsInLocalDb(ArrayList<HashMap> interiorObjectsData, int guideId){
        for (HashMap object: interiorObjectsData) {
            if(object.containsKey("objectId")){
                updateInteriorObjectInLocalDb(object, guideId);
            } else {
                insertInteriorObjectTranslationsToLocalDb(object, guideId);
            }
        }
    }

    public HashMap getDefaultGuideTranslationsFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String, String> guideData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT * FROM guides_translations WHERE guide_id = " + guideId + " AND is_default > 0", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            cursor.moveToFirst();
            guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex("guide_title")));
            guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex("original_guide_title")));
            guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex("description")));
            guideData.put("language", cursor.getString(cursor.getColumnIndex("language")));
        }
        cursor.close();
        return guideData;
    }

    public HashMap getGuideTranslationsFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String, String> guideData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT * FROM guides_translations WHERE guide_id = " + guideId, null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            cursor.moveToFirst();
            guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex("guide_title")));
            guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex("original_guide_title")));
            guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex("description")));
            guideData.put("language", cursor.getString(cursor.getColumnIndex("language")));
        }
        cursor.close();
        return guideData;
    }

    public HashMap getGuideTranslationsFromLocalDb(int guideId, String language){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String, String> guideData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT * FROM guides_translations WHERE guide_id = " + "'" + guideId + "'" +" AND language = " + "'" + language + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            cursor.moveToFirst();
            guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex("guide_title")));
            guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex("original_guide_title")));
            guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex("description")));
        }
        cursor.close();
        return guideData;
    }

    public ArrayList<String> getGuideLanguagesFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> guideLanguagesList = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT language FROM guides_translations WHERE guide_id = " + guideId, null);
        if(cursor == null){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while (cursor.moveToNext()) {
                guideLanguagesList.add(cursor.getString(cursor.getColumnIndex("language")));
            }
        }
        cursor.close();
        return guideLanguagesList;
    }

    public HashMap getGuideFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String, String> guideData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT * FROM guides WHERE guide_id = " + guideId, null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            cursor.moveToFirst();
            guideData.put("username", cursor.getString(cursor.getColumnIndex("username")));
            guideData.put("previewImageName", cursor.getString(cursor.getColumnIndex("preview_image_name")));
            guideData.put("guideType", cursor.getString(cursor.getColumnIndex("guide_type")));
            guideData.put("creationTimestamp", cursor.getString(cursor.getColumnIndex("creation_timestamp")));
            guideData.put("updateTimestamp", cursor.getString(cursor.getColumnIndex("update_timestamp")));
        }
        cursor.close();
        return guideData;
    }

    public HashMap getInteriorFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> mapsNamesList;
        HashMap interiorData = new HashMap();
        Cursor cursor = db.rawQuery("SELECT * FROM interior WHERE guide_id = " + "'" + guideId + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            cursor.moveToFirst();
            interiorData.put("touringTime", cursor.getString(cursor.getColumnIndex("touring_time")));
            interiorData.put("poiId", cursor.getString(cursor.getColumnIndex("poi_id")));
            interiorData.put("poiName", cursor.getString(cursor.getColumnIndex("poi_name")));
            interiorData.put("poiLat", cursor.getDouble(cursor.getColumnIndex("poi_lat")));
            interiorData.put("poiLong", cursor.getDouble(cursor.getColumnIndex("poi_long")));
            mapsNamesList = gson.fromJson(cursor.getString(cursor.getColumnIndex("maps_names_list")), type);
            interiorData.put("mapsNamesList", mapsNamesList);
        }
        cursor.close();
        return interiorData;
    }

    public ArrayList getInteriorObjectsFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap> interiorObjectDataList = new ArrayList<>();
        HashMap<String, String> interiorObjectData;
        Cursor cursor = db.rawQuery("SELECT * FROM interior_object_translations WHERE guide_id = " + "'" + guideId + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                interiorObjectData = new HashMap<>();
                interiorObjectData.put("objectId", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_ID)));
                interiorObjectData.put("objectName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_NAME)));
                interiorObjectData.put("audioFileName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_AUDIO_FILE)));
                interiorObjectData.put("imageFileName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_IMAGE_FILE)));
                interiorObjectData.put("objectDescription", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_DESCRIPTION)));
                interiorObjectData.put("language", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_LANGUAGE)));
                interiorObjectData.put("listPosition", String.valueOf(cursor.getInt(cursor.getColumnIndex(OBJECTS_COLUMN_LIST_POSITION))));
                interiorObjectData.put("pinPosX", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_PIN_X)));
                interiorObjectData.put("pinPosY", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_PIN_Y)));
                interiorObjectData.put("mapName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_MAP_NAME)));
                interiorObjectData.put("guideId", String.valueOf(cursor.getLong(cursor.getColumnIndex(OBJECTS_COLUMN_GUIDE_ID))));
                interiorObjectDataList.add(interiorObjectData);
            }
        }
        cursor.close();
        return interiorObjectDataList;
    }

    public ArrayList getInteriorObjectsFromLocalDb(int guideId, String language){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap> interiorObjectDataList = new ArrayList<>();
        HashMap<String, String> interiorObjectData;
        Cursor cursor = db.rawQuery("SELECT * FROM interior_object_translations WHERE guide_id = " + "'" + guideId + "'" +" AND language = " + "'" + language + "'" + " ORDER BY list_position ASC", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                interiorObjectData = new HashMap<>();
                interiorObjectData.put("objectId", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_ID)));
                interiorObjectData.put("objectName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_NAME)));
                interiorObjectData.put("audioFileName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_AUDIO_FILE)));
                interiorObjectData.put("imageFileName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_IMAGE_FILE)));
                interiorObjectData.put("objectDescription", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_DESCRIPTION)));
                interiorObjectData.put("listPosition", String.valueOf(cursor.getInt(cursor.getColumnIndex(OBJECTS_COLUMN_LIST_POSITION))));
                interiorObjectData.put("pinPosX", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_PIN_X)));
                interiorObjectData.put("pinPosY", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_PIN_Y)));
                interiorObjectData.put("mapName", cursor.getString(cursor.getColumnIndex(OBJECTS_COLUMN_MAP_NAME)));
                interiorObjectDataList.add(interiorObjectData);
            }
        }
        cursor.close();
        return interiorObjectDataList;
    }

    public ArrayList getAllPoiFromLocalDb(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap> poiDataList = new ArrayList<>();
        HashMap<String, String> poiData;
        Cursor cursor = db.rawQuery("SELECT " + INTERIOR_COLUMN_POI_ID + "," + INTERIOR_COLUMN_POI_NAME + "," + INTERIOR_COLUMN_POI_LAT + "," + INTERIOR_COLUMN_POI_LONG + "," + INTERIOR_COLUMN_GUIDE_ID + " FROM interior", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                poiData = new HashMap<>();
                poiData.put("poiId", cursor.getString(cursor.getColumnIndex(INTERIOR_COLUMN_POI_ID)));
                poiData.put("poiName", cursor.getString(cursor.getColumnIndex(INTERIOR_COLUMN_POI_NAME)));
                poiData.put("poiLat", cursor.getString(cursor.getColumnIndex(INTERIOR_COLUMN_POI_LAT)));
                poiData.put("poiLong", cursor.getString(cursor.getColumnIndex(INTERIOR_COLUMN_POI_LONG)));
                poiData.put("guideId", String.valueOf(cursor.getLong(cursor.getColumnIndex(INTERIOR_COLUMN_GUIDE_ID))));
                poiDataList.add(poiData);
            }
        }
        cursor.close();
        return poiDataList;
    }

    public ArrayList getGuidesByPoiIdFromLocalDb(String poiId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Integer> guidesIds = new ArrayList<>();
        HashMap<Integer, String> guideData;
        ArrayList<HashMap> guideTitleById = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT " +  INTERIOR_COLUMN_GUIDE_ID + " FROM interior WHERE " + INTERIOR_COLUMN_POI_ID + " = '" + poiId + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                guidesIds.add(cursor.getInt(cursor.getColumnIndex(INTERIOR_COLUMN_GUIDE_ID)));
            }
        }
        cursor.close();
        for( Integer id : guidesIds){
            guideData = new HashMap<>();
            guideData.put(id, getTitleFromLocalDb(id));
            guideTitleById.add(guideData);
        }
        return guidesIds;
    }

    public String getTitleFromLocalDb(int guideId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT original_guide_title FROM guides_translations WHERE guide_id = " + guideId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor == null) {
            return "Title not found";
        } else {
            String title = "empty";
            while (cursor.moveToNext()) {
                title = cursor.getString(cursor.getColumnIndex("original_guide_title"));
            }
            cursor.close();
            return title;
        }
    }

    public String getPreviewImageNameFromLocalDb(int guideId){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + GUIDES_COLUMN_IMAGE_FILE + " FROM guides WHERE guide_id = " + guideId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor == null) {
            return "Name not found";
        } else {
            String previewImageName = "empty";
            while (cursor.moveToNext()) {
                previewImageName = cursor.getString(cursor.getColumnIndex(GUIDES_COLUMN_IMAGE_FILE));
            }
            cursor.close();
            return previewImageName;
        }
    }

    public int getGuideIdFromLocalDb(String guideTitle) {
        SQLiteDatabase db = this.getReadableDatabase();
        int guideId = 0;
        String query = "SELECT guide_id FROM guides_translations WHERE guide_title = " + "'" + guideTitle + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor == null) {
            return 0;
        } else {
            while (cursor.moveToNext()) {
                guideId = cursor.getInt(cursor.getColumnIndex("guide_id"));
            }
            cursor.close();
            return guideId;
        }
    }

    //function for debugging purposes
    public String getLocalGuidesAsTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        String tableString = String.format("Table local guides\n");
        Cursor cursor = db.rawQuery("SELECT * FROM guides", null);
        if (cursor == null) {
            return "Nic nie ma w bazie";
        } else {
            while (cursor.moveToNext()) {
                String[] columnNames = cursor.getColumnNames();
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name, cursor.getString(cursor.getColumnIndex(name)));
                }
            }
        }
        tableString += "\n";

        cursor.close();
        return tableString;
    }

    public String getLocalGuidesTranslationsAsTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        String tableString = String.format("Table local guides translations\n");
        Cursor cursor = db.rawQuery("SELECT * FROM guides_translations", null);
        if (cursor == null) {
            return "Nic nie ma w bazie";
        } else {
            while (cursor.moveToNext()) {
                String[] columnNames = cursor.getColumnNames();
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name, cursor.getString(cursor.getColumnIndex(name)));
                }
            }
        }
        tableString += "\n";

        cursor.close();
        return tableString;
    }

    public String getLocalInteriorAsTable() {
        SQLiteDatabase db = this.getReadableDatabase();
        String tableString = String.format("Table local interior\n");
        Cursor cursor = db.rawQuery("SELECT * FROM interior", null);
        if (cursor == null) {
            return "Nic nie ma w bazie";
        } else {
            while (cursor.moveToNext()) {
                String[] columnNames = cursor.getColumnNames();
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name, cursor.getString(cursor.getColumnIndex(name)));
                }
            }
        }
        tableString += "\n";

        cursor.close();
        return tableString;
    }

    public String getLocalInteriorObjectsAsTable(){
        SQLiteDatabase db = this.getReadableDatabase();
        String tableString = String.format("Table local interior objects\n");
        Cursor cursor = db.rawQuery("SELECT * FROM interior_object_translations", null);
        if (cursor == null) {
            return "Nic nie ma w bazie";
        } else {
            while (cursor.moveToNext()) {
                String[] columnNames = cursor.getColumnNames();
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name, cursor.getString(cursor.getColumnIndex(name)));
                }
            }
        }
        tableString += "\n";

        cursor.close();
        return tableString;
    }

    public String getLocalCommentssAsTable(){
        SQLiteDatabase db = this.getReadableDatabase();
        String tableString = String.format("Table local comments\n");
        Cursor cursor = db.rawQuery("SELECT * FROM comments", null);
        if (cursor == null) {
            return "Nic nie ma w bazie";
        } else {
            while (cursor.moveToNext()) {
                String[] columnNames = cursor.getColumnNames();
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name, cursor.getString(cursor.getColumnIndex(name)));
                }
            }
        }
        tableString += "\n";

        cursor.close();
        return tableString;
    }

    public Cursor getGuideInfoFromLocalDb() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + GUIDES_TABLE, null);
        if (cursor == null) {
            Log.d("DB ERROR", "CURSOR IS NULL");
        }
        return cursor;
    }

    public Cursor getGuideTranslationsInfoFromLocalDb() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + GUIDES_TRANSLATIONS_TABLE, null);
        if (cursor == null) {
            Log.d("DB ERROR", "CURSOR IS NULL");
        }
        return cursor;
    }

    public Cursor getGuideInfoFromLocalDb(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + GUIDES_TABLE, null); // WHERE username = " + "'" + username + "'", null);
        if (cursor == null) {
            Log.d("DB ERROR", "CURSOR IS NULL");
        }
        return cursor;
    }

    public Cursor getGuideTranslationsInfoFromLocalDb(int guideId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT guide_title, description FROM guides_translations WHERE guide_id = " + "'" + guideId + "'", null);
        if (cursor == null) {
            Log.d("DB ERROR", "CURSOR IS NULL");
        }
        return cursor;
    }

    public void deleteGuideFromLocalDb(int guideId, String username){
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete("guides", "guide_id = ? AND username = ?", new String[]{String.valueOf(guideId), username});
        db.delete("guides_translations", "guide_id = ?", new String[]{String.valueOf(guideId)});
        db.delete("interior", "guide_id = ?", new String[]{String.valueOf(guideId)});
        db.delete("interior_object_translations", "guide_id = ?", new String[]{String.valueOf(guideId)});
        db.delete("comments", "guide_id = ?", new String[]{String.valueOf(guideId)});
    }

    public ArrayList<HashMap> getGuideData(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList guideDataList = new ArrayList<>();
        HashMap guideData;
        Cursor cursor = db.rawQuery("SELECT guides.guide_id, preview_image_name, original_guide_title, guide_title, description, language, is_default FROM " + GUIDES_TABLE + " INNER JOIN " + GUIDES_TRANSLATIONS_TABLE + " ON guides_translations.guide_id = guides.guide_id", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                guideData = new HashMap();
                guideData.put("guideId", cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID)));
                guideData.put("previewImageName", cursor.getString(cursor.getColumnIndex(GUIDES_COLUMN_IMAGE_FILE)));
                guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_TITLE)));
                guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_ORIGINAL_TITLE)));
                guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_DESCRIPTION)));
                guideData.put("language", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_LANGUAGE)));
                guideData.put("isDefault", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_IS_DEFAULT)));
                guideDataList.add(guideData);
            }
        }
        cursor.close();
        return guideDataList;
    }

    public ArrayList<Integer> getGuidesIdsByLanguage(String language){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Integer> guidesIds = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT guide_id FROM guides_translations WHERE language = " + "'" + language + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while(cursor.moveToNext()){
                guidesIds.add(cursor.getInt(cursor.getColumnIndex("guide_id")));
            }
        }
        cursor.close();
        return guidesIds;
    }

    public ArrayList<HashMap> getGuideDataByLanguage(String language){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList guideDataList = new ArrayList<>();
        HashMap guideData;
        Cursor cursor = db.rawQuery("SELECT guides.guide_id, preview_image_name, guide_title, original_guide_title, description, language FROM " + GUIDES_TABLE + " INNER JOIN " + GUIDES_TRANSLATIONS_TABLE + " ON guides_translations.guide_id = guides.guide_id WHERE language = " + "'" + language + "'", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while(cursor.moveToNext()){
                guideData = new HashMap();
                guideData.put("guideId", cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID)));
                guideData.put("previewImageName", cursor.getString(cursor.getColumnIndex(GUIDES_COLUMN_IMAGE_FILE)));
                guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_TITLE)));
                guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_ORIGINAL_TITLE)));
                guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_DESCRIPTION)));
                guideData.put("language", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_LANGUAGE)));
                guideDataList.add(guideData);
            }
        }
        cursor.close();
        return guideDataList;
    }

    public ArrayList<HashMap> getGuideDataById(ArrayList<Integer> guidesIds){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList guideDataList = new ArrayList<>();
        HashMap guideData;
        int size = guidesIds.size();
        int selectedGuideId;
        Cursor cursor = db.rawQuery("SELECT guides.guide_id, preview_image_name, guide_title, original_guide_title, description, language FROM " + GUIDES_TABLE + " INNER JOIN " + GUIDES_TRANSLATIONS_TABLE + " ON guides_translations.guide_id = guides.guide_id WHERE is_default > 0", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while(cursor.moveToNext()){
                selectedGuideId = cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID));
                for (Integer id : guidesIds) {
                    if(selectedGuideId == id){
                        guideData = new HashMap();
                        guideData.put("guideId", selectedGuideId);
                        guideData.put("previewImageName", cursor.getString(cursor.getColumnIndex(GUIDES_COLUMN_IMAGE_FILE)));
                        guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_TITLE)));
                        guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_ORIGINAL_TITLE)));
                        guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_DESCRIPTION)));
                        guideData.put("language", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_LANGUAGE)));
                        guideDataList.add(guideData);
                        break;
                    }
                }
            }
        }
        cursor.close();
        return guideDataList;
    }

    public ArrayList<HashMap> getGuideDataByDefaultLanguage(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList guideDataList = new ArrayList<>();
        HashMap guideData;
        Cursor cursor = db.rawQuery("SELECT guides.guide_id, preview_image_name, guide_title, original_guide_title, description, language FROM " + GUIDES_TABLE + " INNER JOIN " + GUIDES_TRANSLATIONS_TABLE + " ON guides_translations.guide_id = guides.guide_id WHERE is_default > 0", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while(cursor.moveToNext()){
                guideData = new HashMap();
                guideData.put("guideId", cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID)));
                guideData.put("previewImageName", cursor.getString(cursor.getColumnIndex(GUIDES_COLUMN_IMAGE_FILE)));
                guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_TITLE)));
                guideData.put("originalGuideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_ORIGINAL_TITLE)));
                guideData.put("guideDescription", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_DESCRIPTION)));
                guideData.put("language", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_LANGUAGE)));
                guideDataList.add(guideData);
            }
        }
        cursor.close();
        return guideDataList;
    }

    public ArrayList<HashMap> getGuideData(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList guideDataList = new ArrayList<>();
        HashMap guideData;
        Cursor cursor = db.rawQuery("SELECT guides.guide_id, guide_title FROM " + GUIDES_TABLE + " INNER JOIN " + GUIDES_TRANSLATIONS_TABLE + " ON guides_translations.guide_id = guides.guide_id WHERE username = " + "'" + username + "'" + " AND is_default > 0", null);
        if( cursor == null ){
            Log.d("DB ERROR", "CURSOR IS NULL");
        } else {
            while( cursor.moveToNext()){
                guideData = new HashMap();
                guideData.put("guideId", cursor.getInt(cursor.getColumnIndex(GUIDES_COLUMNN_ID)));
                guideData.put("guideTitle", cursor.getString(cursor.getColumnIndex(GUIDES_TRANSLATIONS_TITLE)));
                guideDataList.add(guideData);
            }
        }
        cursor.close();
        return guideDataList;
    }
}
