package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;

import com.android.volley.toolbox.JsonObjectRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.SharedPreferencesClass;

public class InteriorPresenterImpl implements InteriorContract.InteriorPresenter, InteriorContract.InteriorPresenterOps{

    private InteriorContract.InteriorView interiorView;
    private InteriorContract.InteriorModel interiorModel;

    private String mainFolder = "GuideMaker";
    private String guideName;
    private boolean itemsSwapped = false;
    private FileManager fileManager = new FileManager();
    private static final String KEY_OBJECT_NAME = "objectName";
    private static final String KEY_TOURING_TIME = "touringTime";
    private static final String KEY_POI_NAME = "poiName";
    private static final String KEY_MAPS_NAMES = "mapsNamesList";

    private SharedPreferencesClass preferences;




    public InteriorPresenterImpl(InteriorContract.InteriorView interiorView, Context context){
        this.interiorView = interiorView;
        this.interiorModel = new InteriorModelImpl(this, context);
        preferences = new SharedPreferencesClass(context);
        guideName = preferences.getGuideName();
    }

    @Override
    public void passPoiInfo(String poiName, String poiId, double poiLongitude, double poiLatitude){
        interiorModel.addPoiToInteriorDataMap(poiName, poiId, poiLongitude, poiLatitude);
    }

    @Override
    public void addObjectToList(HashMap objectData){
        interiorModel.addObjectToArray(objectData);
    }

    @Override
    public int getExifOrientation(String imagePath) {
        return fileManager.getExifOrientation(imagePath);
    }

    public void copyMapFile(File mapFile){
        File map = new File(Environment.getExternalStorageDirectory() + "/" + mainFolder + "/" + guideName, mapFile.getName());
        try {
            fileManager.copyFile(mapFile, map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Intent handleSaveButton(){
        interiorModel.addInteriorData(interiorView.getEtTouringTime(), interiorView.getMapsNamesList());
        Intent intent = new Intent();
        intent.putExtra("objectsArray", interiorModel.getObjectsArrayList());
        intent.putExtra("interiorData", interiorModel.getInteriorData());
        intent.putExtra("itemsSwapped", itemsSwapped);
        return intent;
    }

    @Override
    public void onAddingSuccess(String message){
        interiorView.showToastMessage(message);
    }

    @Override
    public void onAddingFail(String message){
        interiorView.showToastMessage(message);
    }

    public HashMap getObjectDataFromList(int position){
        return interiorModel.getObjectFromArrayList(position);
    }

    public void editObjectInArray(int index, HashMap objectData){
        interiorModel.editObjectInArray(index, objectData);
    }

    public void swapItemsInArray(int item1, int item2){
        interiorModel.swapItemsInArray(item1, item2);
        itemsSwapped = true;
    }

    @Override
    public void deleteObjectFromArray(int index){
        interiorModel.deleteObjectFromArray(index);
    }

    public ArrayList<String> getObjectNameFromList(){
        ArrayList<String> objectsNames = new ArrayList<>();
        ArrayList<HashMap> objects = interiorModel.getObjectsArrayList();
        int size = interiorModel.getObjectsArrayList().size();
        for ( int i = 0; i < size; i++ ){
            objectsNames.add((String) objects.get(i).get(KEY_OBJECT_NAME));
        }
        return objectsNames;
    }

    @Override
    public void passJsObjectRequest(JsonObjectRequest jsObjectRequest){
        interiorView.accessSingleton(jsObjectRequest);
    }

    @Override
    public void loadInteriorData(HashMap data){
        interiorModel.setInteriorData(data);
        if( data != null && data.size() > 0 ) {
            interiorView.setEtTouringTime((String) data.get(KEY_TOURING_TIME));
            interiorView.setTvLocation((String) data.get(KEY_POI_NAME));
            interiorView.setMapsNamesList((ArrayList<String>) data.get(KEY_MAPS_NAMES));
        }
    }

    @Override
    public void loadInteriorObjectsData(ArrayList<HashMap<String, String>> data){
        interiorModel.setObjectsList(data);
    }

    public String getGuideName(){
        return guideName;
    }
}
