package project.app.mobile.guidemaker.viewGuide;

import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.ImageFragment;
import project.app.mobile.guidemaker.R;

public class ShowMaps extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_maps);

        Bundle extras = getIntent().getExtras();
        ArrayList<String> mapsNamesList = extras.getStringArrayList("mapsNamesList");
        String originalGuideTitle = extras.getString("originalGuideTitle");

        TabLayout tabLayout = findViewById(R.id.tabLayoutShowMaps);
        int listSize = mapsNamesList.size();
        for (int i = 0; i < listSize; i++) {
            String tabName = mapsNamesList.get(i);
            tabLayout.addTab(tabLayout.newTab().setText(tabName));
        }
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        int tabCount = tabLayout.getTabCount();

        final ViewPager viewPager = findViewById(R.id.viewPagerObjectShowMaps);
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mapsNamesList, true, tabCount, originalGuideTitle);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}
