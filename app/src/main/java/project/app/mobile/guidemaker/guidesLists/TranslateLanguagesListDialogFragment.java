package project.app.mobile.guidemaker.guidesLists;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import project.app.mobile.guidemaker.Languages;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.translate.TranslateGuideActivity;

public class TranslateLanguagesListDialogFragment extends DialogFragment {

    private OnFragmentInteractionListener mListener;
    private int guideId;
    private String guideTitle;
    private ArrayList<String> supportedLanguages;
    private Languages languages;
    private ArrayList<String> translatedLanguagesList;
    private ArrayList<String> untranslatedLanguagesList;
    private String translateFrom;
    private String translateTo;

    private Button btnNext;

    public TranslateLanguagesListDialogFragment() {
        // Required empty public constructor
    }

    public static TranslateLanguagesListDialogFragment newInstance(int guideId, String guideTitle, ArrayList<String> translatedLanguagesList) {
        TranslateLanguagesListDialogFragment fragment = new TranslateLanguagesListDialogFragment();
        Bundle args = new Bundle();
        args.putInt("guideId", guideId);
        args.putString("guideTitle", guideTitle);
        args.putStringArrayList("languages", translatedLanguagesList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_translate_languages_list, container, false);

        guideId = getArguments().getInt("guideId");
        guideTitle = getArguments().getString("guideTitle");
        translatedLanguagesList = getArguments().getStringArrayList("languages");
        languages = new Languages();
        supportedLanguages = languages.getSupportedLanguages();
        untranslatedLanguagesList = new ArrayList<>();

        final RadioGroup translateFromRadioGroup = rootView.findViewById(R.id.translateFromRadioGroup);
        final RadioGroup translateToRadioGroup = rootView.findViewById(R.id.translateToRadioGroup);
        btnNext = rootView.findViewById(R.id.translateListNextButton);
        createUntranslatedLanguagesList();
        createTranslateToRadioButtons(translateToRadioGroup);

        createTranslateFromRadioButtons(translateFromRadioGroup);

        translateFromRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton button = rootView.findViewById(i);
                translateFrom = button.getText().toString();
            }
        });

        translateToRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton button = rootView.findViewById(i);
                translateTo = button.getText().toString();
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), TranslateGuideActivity.class);
                i.putExtra("guideId", guideId);
                i.putExtra("guideTitle", guideTitle);
                i.putExtra("translateFrom", translateFrom);
                i.putExtra("translateTo", translateTo);
                startActivity(i);
            }
        });
        return rootView;
    }

    private void createTranslateToRadioButtons(RadioGroup radioGroup){
        int size = untranslatedLanguagesList.size();
        if(size == 0){
            btnNext.setVisibility(View.GONE);
            RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setId(0);
            radioButton.setText(getContext().getResources().getString(R.string.no_languages));
            radioGroup.addView(radioButton);
        } else {
            RadioButton[] radioButton = new RadioButton[size];
            for (int i = 0; i < size; i++) {
                radioButton[i] = new RadioButton(getActivity());
                radioButton[i].setId(i);
                radioButton[i].setText(untranslatedLanguagesList.get(i));
                radioGroup.addView(radioButton[i]);
            }
        }
    }

    private void createTranslateFromRadioButtons(RadioGroup radioGroup){
        int size = translatedLanguagesList.size();
        RadioButton[] radioButton = new RadioButton[size];
        for(int i = 0; i < size; i++){
            radioButton[i] = new RadioButton(getActivity());
            radioButton[i].setId(i+ 1000);
            radioButton[i].setText(translatedLanguagesList.get(i));
            radioGroup.addView(radioButton[i]);
        }
    }

    private void createUntranslatedLanguagesList(){
        for(String language : supportedLanguages){
            if(!translatedLanguagesList.contains(language)){
                untranslatedLanguagesList.add(language);
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
