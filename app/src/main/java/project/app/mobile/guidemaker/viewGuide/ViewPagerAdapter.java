package project.app.mobile.guidemaker.viewGuide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.ImageFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private int numberOfTabs;
    private int guideId;
    private ArrayList<HashMap<String, String>> interiorObjectsDataList;
    private String originalGuideTitle;
    private String language;
    private ArrayList<String> mapsNamesList;
    private boolean showMaps;

    public ViewPagerAdapter(FragmentManager fm, int numberOfTabs,
                            ArrayList<HashMap<String, String>> interiorObjectsDataList,
                            String originalGuideTitle,
                            int guideId, String language){
        super(fm);
        this.numberOfTabs = numberOfTabs;
        this.interiorObjectsDataList = interiorObjectsDataList;
        this.originalGuideTitle = originalGuideTitle;
        this.guideId = guideId;
        this.language = language;
    }

    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> mapsNamesList, boolean showMaps, int numberOfTabs, String originalGuideTitle){
        super(fm);
        this.mapsNamesList = mapsNamesList;
        this.showMaps = showMaps;
        this.numberOfTabs = numberOfTabs;
        this.originalGuideTitle = originalGuideTitle;
    }

    @Override
    public Fragment getItem(int position) {
        if(showMaps){
            Fragment fragment = new ImageFragment().newInstance(mapsNamesList.get(position), originalGuideTitle);
            return fragment;
        } else if( position == numberOfTabs - 1){
            CommentFragment fragment = new CommentFragment(guideId);
            return  fragment;
        } else {
            String objectName = interiorObjectsDataList.get(position).get("objectName");
            String objectDescription = interiorObjectsDataList.get(position).get("objectDescription");
            String objectImageName = interiorObjectsDataList.get(position).get("imageFileName");
            String objectAudioName = interiorObjectsDataList.get(position).get("audioFileName");
            String objectPinPosX = interiorObjectsDataList.get(position).get("pinPosX");
            String objectPinPosY = interiorObjectsDataList.get(position).get("pinPosY");
            String mapName = interiorObjectsDataList.get(position).get("mapName");
            Fragment fragment = new ObjectFragment().newInstance(objectName, objectDescription, objectImageName, objectAudioName, mapName, originalGuideTitle, language, objectPinPosX, objectPinPosY);
            return  fragment;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
