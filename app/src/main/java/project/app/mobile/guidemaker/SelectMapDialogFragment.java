package project.app.mobile.guidemaker;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import project.app.mobile.guidemaker.interior.AddPoiFragment;


public class SelectMapDialogFragment extends Fragment {

    private ArrayList<String> mapsList;
    private String selectedMap;
    private String guideName;
    private FragmentManager fragmentManager;

    private OnFragmentInteractionListener mListener;

    public SelectMapDialogFragment() {
        // Required empty public constructor
    }

    public static SelectMapDialogFragment newInstance(ArrayList<String> mapsList, String guideName) {
        SelectMapDialogFragment fragment = new SelectMapDialogFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("mapsList", mapsList);
        args.putString("guideName", guideName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mapsList = getArguments().getStringArrayList("mapsList");
            guideName = getArguments().getString("guideName");
        }
        fragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_select_map_dialog, container, false);

        RadioGroup mapsRadioGroup = rootView.findViewById(R.id.mapsDialogRadioGroup);
        Button nextButton = rootView.findViewById(R.id.mapsDialogNextButton);

        createRadioButtons(mapsRadioGroup);

        mapsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton button = rootView.findViewById(i);
                selectedMap = button.getText().toString();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedMap != null) {
                    Fragment fragment = new ImageFragment().newInstance(selectedMap, guideName, 0, null, null);
                    fragmentManager = getChildFragmentManager();
                    fragmentManager.beginTransaction().add(R.id.selectMapDialogFragment, fragment, "dialogFragment").addToBackStack("selectMap").commit();
                }
            }
        });


        return rootView;
    }

    private void createRadioButtons(RadioGroup radioGroup){
        int size = mapsList.size();
        RadioButton[] radioButton = new RadioButton[size];
        for(int i = 0; i < size; i++){
            radioButton[i] = new RadioButton(getActivity());
            radioButton[i].setId(i);
            radioButton[i].setText(mapsList.get(i));
            radioGroup.addView(radioButton[i]);
        }
    }

    public void passPinAndMapName(double pinX, double pinY, String mapName){
        ((AddPoiFragment) getParentFragment()).setPinPosAndMapName(pinX, pinY, mapName);
    }

    public void closeImage(){
        ((AddPoiFragment) getParentFragment()).closeImage();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
