package project.app.mobile.guidemaker.viewGuide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import java.io.File;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.ImageFragment;
import project.app.mobile.guidemaker.Languages;
import project.app.mobile.guidemaker.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ObjectFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class ObjectFragment extends Fragment {

    private final static int CHECK_TTS_INTENT = 1;

    private FileManager fileManager = new FileManager();
    private MediaPlayer mediaPlayer;
    private Handler seekBarUpdateHandler = new Handler();

    private TextView tvObjectDescription;
    private TextView tvAudioTime;
    private TextView tvObjectName;
    private ImageView imObjectImage;
    private SeekBar audioSeekBar;
    private ImageButton playAudioButton;
    private ImageButton pauseAudioButton;
    private Button btnShowScheme;
    private String objectName;
    private String objectDescription;
    private String objectImageName;
    private String objectAudioName;
    private String durationText;
    private String mapName;
    private String originalGuideTitle;
    private String language;
    private String pinPosX;
    private String pinPosy;
    private int timePlayed;

    private FragmentManager fragmentManager;
    private OnFragmentInteractionListener mListener;

    private TextToSpeech tts;
    private File synthesizedFile;
    private String audioPath;
    private Languages languages;
    private boolean isViewShown = false;

    public ObjectFragment() {
        // Required empty public constructor
    }

    public static ObjectFragment newInstance(String objectName, String objectDescription, String objectImageName, String objectAudioName, String mapName, String originalGuideTitle, String language, String pinPosX, String pinPosY){
        ObjectFragment fragment = new ObjectFragment();

        Bundle args = new Bundle();
        args.putString("OBJECT_NAME", objectName);
        args.putString("OBJECT_DESCRIPTION", objectDescription);
        args.putString("OBJECT_IMAGE_NAME", objectImageName);
        args.putString("OBJECT_AUDIO_NAME", objectAudioName);
        args.putString("MAP_NAME", mapName);
        args.putString("ORIGINAL_GUIDE_TITLE", originalGuideTitle);
        args.putString("LANGUAGE", language);
        args.putString("PIN_POS_X", pinPosX);
        args.putString("PIN_POS_Y", pinPosY);

        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("ValidFragment")
    public ObjectFragment(String guideTitle, String objectName,
                          String objectDescription, String objectImageName,
                          String objectAudioName, String mapName,
                          String originalGuideTitle, String language) {
        this.objectName = objectName;
        this.objectDescription = objectDescription;
        this.objectImageName = objectImageName;
        this.objectAudioName = objectAudioName;
        this.mapName = mapName;
        this.originalGuideTitle = originalGuideTitle;
        this.language = language;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedInstanceState = getArguments();
        objectName = savedInstanceState.getString("OBJECT_NAME");
        objectDescription = savedInstanceState.getString("OBJECT_DESCRIPTION");
        objectImageName = savedInstanceState.getString("OBJECT_IMAGE_NAME");
        objectAudioName = savedInstanceState.getString("OBJECT_AUDIO_NAME");
        mapName = savedInstanceState.getString("MAP_NAME");
        originalGuideTitle = savedInstanceState.getString("ORIGINAL_GUIDE_TITLE");
        language = savedInstanceState.getString("LANGUAGE");
        pinPosX = savedInstanceState.getString("PIN_POS_X");
        pinPosy = savedInstanceState.getString("PIN_POS_Y");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_object, container, false);

        fragmentManager = getFragmentManager();
        tvObjectName = rootView.findViewById(R.id.viewGuideObjectName);
        tvObjectName.setText(objectName);
        btnShowScheme = rootView.findViewById(R.id.schemeButton);
        playAudioButton = rootView.findViewById(R.id.playButton);
        pauseAudioButton = rootView.findViewById(R.id.pauseButton);
        audioSeekBar = rootView.findViewById(R.id.audioSeekBar);
        tvAudioTime = rootView.findViewById(R.id.audioTime);
        tvObjectDescription = rootView.findViewById(R.id.viewObjectDescription);
        tvObjectDescription.setText(objectDescription);
        imObjectImage = rootView.findViewById(R.id.viewImageObject);

        Thread thread = new Thread() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadImage();
                        mediaPlayer = new MediaPlayer();
                        languages = new Languages();
                    }
                });
            }
        };
        thread.start();


        audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Intent checkTtsIntent = new Intent();
        checkTtsIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTtsIntent, CHECK_TTS_INTENT);

        btnShowScheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ImageFragment().newInstance(mapName, originalGuideTitle, 12345, pinPosX, pinPosy);
                fragmentManager.beginTransaction().replace(R.id.object_fragment, fragment, "objectFragment").addToBackStack(null).commit();
            }
        });


        playAudioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudioButton.setVisibility(View.GONE);
                //pauseAudioButton.setVisibility(View.VISIBLE);
                playAudioFile();
            }
        });

        pauseAudioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pauseAudioButton.setVisibility(View.GONE);
                playAudioButton.setVisibility(View.VISIBLE);
                pauseAudioFile();
            }
        });


        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null){
            isViewShown = true;
            Intent checkTtsIntent = new Intent();
            checkTtsIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkTtsIntent, CHECK_TTS_INTENT);
        } else {
            isViewShown = false;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(!getObjectDescription().equals("") && objectAudioName.equals("")) {
            if(requestCode == CHECK_TTS_INTENT){
                initializeTts();
            } else {
                Intent installTtsData = new Intent();
                installTtsData.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTtsData);
            }
        }
    }

    public void initializeTts(){
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                int isLanguageAvailable = tts.isLanguageAvailable(new Locale(languages.getLanguageCode(language)));
                switch (isLanguageAvailable){
                    case TextToSpeech.LANG_AVAILABLE:
                    tts.setLanguage(new Locale(languages.getLanguageCode(language)));
                    synthesizedFile = new File(Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + originalGuideTitle, objectName);
                    tts.synthesizeToFile(getObjectDescription(), null, synthesizedFile, null);
                    break;
                    case TextToSpeech.LANG_NOT_SUPPORTED:
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private String getObjectDescription(){
        return tvObjectDescription.getText().toString();
    }

    private void playAudioFile(){
        if(!new File(Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + originalGuideTitle + "/" + objectName).exists()) {
            audioPath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + originalGuideTitle + "/" + objectAudioName;
        } else {
            audioPath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + originalGuideTitle + "/" + objectName;
        }
        if( mediaPlayer.getCurrentPosition() > 0 ){
            mediaPlayer.start();
        } else {
            try {
                mediaPlayer.setDataSource(audioPath);
                mediaPlayer.prepare();
                int audioDuration = mediaPlayer.getDuration();
                audioSeekBar.setMax(audioDuration);
                durationText = String.format(Locale.ROOT,"%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(audioDuration), TimeUnit.MILLISECONDS.toSeconds(audioDuration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(audioDuration)));
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        seekBarUpdateHandler.postDelayed(updateSeekBar, 0);
    }

    private void pauseAudioFile(){
        mediaPlayer.pause();
        seekBarUpdateHandler.removeCallbacks(updateSeekBar);
    }

    private Runnable updateSeekBar = new Runnable() {
        @SuppressLint("SetTextI18n")
        @Override
        public void run() {
            timePlayed = mediaPlayer.getCurrentPosition();
            String timePlayedText = String.format(Locale.ROOT,"%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(timePlayed), TimeUnit.MILLISECONDS.toSeconds(timePlayed) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timePlayed)));
            tvAudioTime.setText(timePlayedText + "/" + durationText);
            audioSeekBar.setProgress(timePlayed);
            seekBarUpdateHandler.postDelayed(this, 50);
        }
    };

    private void loadImage(){
        String imagePath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + originalGuideTitle + "/" + objectImageName;
        int rotation = fileManager.getExifOrientation(imagePath);
        File imageFile = new File(imagePath);
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        imObjectImage.setImageBitmap(fileManager.loadImage(getActivity(), rotation, bitmap));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
