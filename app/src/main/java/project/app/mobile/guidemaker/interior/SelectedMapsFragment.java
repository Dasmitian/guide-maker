package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import project.app.mobile.guidemaker.R;

public class SelectedMapsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private ArrayList<String> mapsNamesList;
    private String guideName;
    private ListView mapsListView;

    public SelectedMapsFragment() {
        // Required empty public constructor
    }

    public static SelectedMapsFragment newInstance(ArrayList<String> mapsNamesList, String guideName) {
        SelectedMapsFragment fragment = new SelectedMapsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("mapsNameList", mapsNamesList);
        args.putString("guideName", guideName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mapsNamesList = getArguments().getStringArrayList("mapsNameList");
            guideName = getArguments().getString("guideName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_selected_maps, container, false);
        mapsListView = rootView.findViewById(R.id.mapsList);
        createList();

        return rootView;
    }

    private void createList(){
        ObjectMapsAdapter adapter = new ObjectMapsAdapter(mapsNamesList, guideName);
        mapsListView.setAdapter(adapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
