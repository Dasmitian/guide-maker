package project.app.mobile.guidemaker.interior;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.SessionHandler;
import project.app.mobile.guidemaker.User;

public class InteriorModelImpl implements  InteriorContract.InteriorModel {

    private InteriorContract.InteriorPresenterOps presenter;
    private LocalSqliteDb localDb;

    private static final String KEY_TOURING_TIME = "touringTime";
    private static final String KEY_POI_NAME = "poiName";
    private static final String KEY_POI_ID = "poiId";
    private static final String KEY_POI_LAT = "poiLat";
    private static final String KEY_POI_LONG = "poiLong";
    private static final String KEY_GUIDENAME = "guidename";
    private static final String KEY_MAPS_NAMES = "mapsNamesList";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_STATUS = "status";
    private String addInteriorGuide_url = "http://guidemaker.gearhostpreview.com/php/add_interior_guide.php";

    private ArrayList<HashMap<String, String>> objectsList;
    private HashMap interiorData;

    private User user;
    private SessionHandler session;
    private Context context;

    public InteriorModelImpl(InteriorContract.InteriorPresenterOps presenter, Context context){
        this.presenter = presenter;
        session = new SessionHandler(context);
        user = session.getUserDetails();
        objectsList = new ArrayList<>();
        interiorData = new HashMap<>();
        this.context = context;
        localDb = new LocalSqliteDb(context, "localDatabase.db");
    }

    @Override
    public void addObjectToArray(HashMap objectData){
        int position;
        int size = objectsList.size();
        if(size == 0){
            position = 0;
        } else {
            position = size;
        }
        objectData.put("listPosition", position);
        objectsList.add(objectData);
    }

    @Override
    public void editObjectInArray(int index, HashMap objectData){
        objectsList.set(index, objectData);
    }

    public void swapItemsInArray(int item1, int item2){
        String item1Position;
        String item2Position;
        try {
            item1Position = objectsList.get(item1).get("listPosition");
            item2Position = objectsList.get(item2).get("listPosition");
        } catch (ClassCastException e){

        } finally {
            item1Position = String.valueOf(objectsList.get(item1).get("listPosition"));
            item2Position = String.valueOf(objectsList.get(item2).get("listPosition"));
        }
        objectsList.get(item1).put("listPosition", item2Position);
        objectsList.get(item2).put("listPosition", item1Position);
        localDb.updateInteriorObjectPosition(objectsList.get(item1));
        localDb.updateInteriorObjectPosition(objectsList.get(item2));
    }

    @Override
    public void deleteObjectFromArray(int index){
        objectsList.remove(index);
    }

    @Override
    public HashMap getObjectFromArrayList(int index){
        return objectsList.get(index);
    }

    @Override
    public ArrayList getObjectsArrayList(){
        return objectsList;
    }

    @Override
    public void addInteriorGuideToList(String touringTime, String guidename){
        JSONObject request = new JSONObject();
        try {
            request.put(KEY_TOURING_TIME, touringTime);
            request.put(KEY_GUIDENAME, guidename);
            request.put(KEY_USERNAME, user.getUsername());
            request.put(KEY_MAPS_NAMES, "interiorMap.jpg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjectRequest = new JsonObjectRequest(Request.Method.POST, addInteriorGuide_url, request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt(KEY_STATUS) == 0) {
                        presenter.onAddingSuccess(response.getString(KEY_MESSAGE));
                    } else if (response.getInt(KEY_STATUS) == 1) {
                        presenter.onAddingFail(response.getString(KEY_MESSAGE));
                    } else {
                        presenter.onAddingFail(response.getString(KEY_MESSAGE));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        presenter.passJsObjectRequest(jsObjectRequest);
    }

    @Override
    public HashMap getInteriorData(){
        return interiorData;
    }

    @Override
    public void addPoiToInteriorDataMap(String poiName, String poiId, double poiLong, double poiLat){
        interiorData.put(KEY_POI_NAME, poiName);
        interiorData.put(KEY_POI_ID, poiId);
        interiorData.put(KEY_POI_LAT, Double.toString(poiLat));
        interiorData.put(KEY_POI_LONG, Double.toString(poiLong));
    }

    @Override
    public void addInteriorData(String touringTime, ArrayList<String> mapsNamesList){
        interiorData.put(KEY_TOURING_TIME, touringTime);
        interiorData.put(KEY_MAPS_NAMES, mapsNamesList);
    }

    @Override
    public void setInteriorData(HashMap data){
        interiorData = data;
    }

    @Override
    public void setObjectsList(ArrayList<HashMap<String, String>> data){
        objectsList = data;
    }
}
