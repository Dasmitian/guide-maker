package project.app.mobile.guidemaker.guidesLists;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.createGuide.CreateGuideActivity;

public class DownloadedGuidesListAdapter extends BaseAdapter implements ListAdapter {
    private Cursor cursorGuidesData;
    private Cursor cursorGuidesTranslationsData;
    private Context context;
    private File imageFile;
    private String imagePath;
    private int rotation = 0;
    private FileManager fileManager;
    private String folderName;
    private String imageName;
    private Bitmap bitmap;
    private ArrayList<HashMap> guideDataList;

    public DownloadedGuidesListAdapter(ArrayList<HashMap> guideDataList, Context context){
        this.guideDataList = guideDataList;
        this.context = context;
        fileManager = new FileManager();
    }

    @Override
    public int getCount() {
        return guideDataList.size();
    }

    @Override
    public String getItem(int position) {
        return guideDataList.get(position).get("language").toString();
    }

    @Override
    public long getItemId(int position) {
        return  Integer.parseInt(String.valueOf(guideDataList.get(position).get("guideId")));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.local_guides_list, null);
        }

        ImageView previewImage = view.findViewById(R.id.previewImageView);
        TextView guideTitle = view.findViewById(R.id.guideTitle);
        TextView guideDescription = view.findViewById(R.id.guideDescription);

        folderName = guideDataList.get(position).get("originalGuideTitle").toString();
        imageName = guideDataList.get(position).get("previewImageName").toString();

        guideTitle.setText(folderName);
        guideDescription.setText(guideDataList.get(position).get("guideDescription").toString());

        imagePath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + folderName + "/" + imageName;
        rotation = fileManager.getExifOrientation(imagePath);
        imageFile = new File(imagePath);
        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        previewImage.setImageBitmap(fileManager.loadImage(context, rotation, bitmap));

        return view;
    }

    void updateAdapter(ArrayList<HashMap> newGuideDataList){
        guideDataList.clear();
        guideDataList.addAll(newGuideDataList);
        this.notifyDataSetChanged();
    }
}
