package project.app.mobile.guidemaker;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;


public class SelectLanguageDialogFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Languages languages;
    private ArrayList<String> supportedLanguages;
    private ArrayList<String> availableLanguages;
    private String chosenLanguage;
    private OnLanguageSelectionListener onLanguageSelectionListener;

    public SelectLanguageDialogFragment() {
        // Required empty public constructor
    }

    public SelectLanguageDialogFragment(ArrayList<String> availableLanguages) {
        this.availableLanguages = availableLanguages;
    }

    public static SelectLanguageDialogFragment newInstance() {
        SelectLanguageDialogFragment fragment = new SelectLanguageDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languages = new Languages();
        supportedLanguages = languages.getSupportedLanguages();
        onAttachToParentFragment(getParentFragment());
        onAttachToParentActivity(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_select_language_dialog, container, false);

        RadioGroup languagesRadioGroup = rootView.findViewById(R.id.supportedLanguagesRadioGroup);
        if(getActivity().getClass().getSimpleName().equals("CreateGuideActivity")) {
            createSupportedRadioButtons(languagesRadioGroup);
        } else if(getActivity().getClass().getSimpleName().equals("ViewGuideActivity")){
            createAvailableRadioButtons(languagesRadioGroup);
        } else {
            createSupportedRadioButtons(languagesRadioGroup);
        }

        languagesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton button = rootView.findViewById(i);
                chosenLanguage = button.getText().toString();
                onLanguageSelectionListener.onLanguageSelection(chosenLanguage);
                getFragmentManager().popBackStack();
            }
        });

        return rootView;
    }

    private void createAvailableRadioButtons(RadioGroup radioGroup){
        int size = availableLanguages.size();
        RadioButton[] radioButton = new RadioButton[size];
        for(int i = 0; i < size; i++){
            radioButton[i] = new RadioButton(getActivity());
            radioButton[i].setId(i);
            radioButton[i].setText(availableLanguages.get(i));
            radioGroup.addView(radioButton[i]);
        }
    }

    private void createSupportedRadioButtons(RadioGroup radioGroup){
        int size = supportedLanguages.size();
        RadioButton[] radioButton = new RadioButton[size];
        for(int i = 0; i < size; i++){
            radioButton[i] = new RadioButton(getActivity());
            radioButton[i].setId(i);
            radioButton[i].setText(supportedLanguages.get(i));
            radioGroup.addView(radioButton[i]);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public interface OnLanguageSelectionListener{
        void onLanguageSelection(String chosenLanguage);
    }

    public void onAttachToParentFragment(Fragment fragment){
        onLanguageSelectionListener = (OnLanguageSelectionListener)fragment;
    }

    public void onAttachToParentActivity(Activity activity){
        if(activity.getClass().getSimpleName().equals("CreateGuideActivity")) {
            onLanguageSelectionListener = (OnLanguageSelectionListener) activity;
        } else if(activity.getClass().getSimpleName().equals("ViewGuideActivity")){
            onLanguageSelectionListener = (OnLanguageSelectionListener) activity;
        }
    }
}
