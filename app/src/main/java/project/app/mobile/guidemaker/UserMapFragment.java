package project.app.mobile.guidemaker;

import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import project.app.mobile.guidemaker.viewGuide.ViewGuideActivity;

public class UserMapFragment extends Fragment {

    MapView mapView;
    private GoogleMap map;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private final LatLng mDefaultLocation = new LatLng(53.123407851013425, 18.007944971323013);
    private Location lastKnownLocation;
    private CameraPosition cameraposition;
    private Marker locationMarker;
    private LocalSqliteDb localDb;
    private ArrayList<HashMap> poiDataList;
    private Location currentLocation;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private List<Marker> markers;
    private int range = 400;
    private int defaultRange = 400;
    private SeekBar skbrRange;
    private Button btnRange;
    private TextView tvRangeMin;
    private TextView tvRangeMax;
    private TextView tvRangeCurrent;
    private Circle rangeCircle;
    private LatLng latLng;
    private boolean showRange = false;

    private String guidesForObject;
    private String viewString;
    private String currentString;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View rootView = inflater.inflate(R.layout.fragment_user_map, container, false);
        if( savedInstanceState != null){
            lastKnownLocation = savedInstanceState.getParcelable("location");
            cameraposition = savedInstanceState.getParcelable("camera_position");
        }

        btnRange = rootView.findViewById(R.id.rangeButton);
        skbrRange = rootView.findViewById(R.id.rangeSeekBar);
        tvRangeMin = rootView.findViewById(R.id.rangeMinTxt);
        tvRangeMax = rootView.findViewById(R.id.rangeMaxTxt);
        tvRangeCurrent = rootView.findViewById(R.id.rangeCurrent);

        guidesForObject = getResources().getString(R.string.guides_for_object);
        viewString = getResources().getString(R.string.view);
        currentString = getResources().getString(R.string.current);

        localDb = new LocalSqliteDb(getActivity(), "localDatabase.db");
        poiDataList = localDb.getAllPoiFromLocalDb();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mapView = rootView.findViewById(R.id.mapUser);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style_json));
                locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        rangeCircle.remove();
                        drawCircle(range);

                        hideOrShowMarkers(400);
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }
                };
                getLocationPermission();
                if (checkLocationPermission()) {
                    if(latLng == null){
                        latLng = mDefaultLocation;
                    } else {
                        latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    }
                    updateLocation();
                    getDeviceLocation();
                    placeMarkers();
                    drawCircle(defaultRange);

                    skbrRange.setMax(10000);
                    skbrRange.setProgress(defaultRange);

                    hideOrShowMarkers(range);


                    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng2) {
                        }
                    });

                    map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            Dialog dialog = new Dialog(getActivity());
                            dialog.setTitle(guidesForObject);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            TableLayout layout = new TableLayout(getActivity());
                            dialog.setContentView(layout);
                            ArrayList<Integer> guideIds = localDb.getGuidesByPoiIdFromLocalDb((String) marker.getTag());
                            TableRow poiNameRow = new TableRow(getActivity());
                            TextView tvPoiName = new TextView(getActivity());
                            tvPoiName.setText(marker.getTitle());
                            poiNameRow.addView(tvPoiName);
                            layout.addView(poiNameRow);
                            int arraySize = guideIds.size();

                            for (int i = 0; i < arraySize; i++) {
                                TableRow row = new TableRow(getActivity());
                                TextView tvGuideTitle = new TextView(getActivity());
                                final Button btnView = new Button(getActivity());
                                btnView.setText(viewString);
                                btnView.setId(guideIds.get(i));
                                String text = localDb.getTitleFromLocalDb(guideIds.get(i));
                                tvGuideTitle.setText(text);
                                row.addView(tvGuideTitle);
                                row.addView(btnView);
                                layout.addView(row);

                                btnView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), ViewGuideActivity.class);
                                        intent.putExtra("guideId", btnView.getId());
                                        startActivity(intent);
                                    }
                                });
                            }
                            dialog.show();
                            return false;
                        }
                    });

                }
            }
        });

        btnRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( !showRange ) {
                    skbrRange.setVisibility(View.VISIBLE);
                    tvRangeMin.setVisibility(View.VISIBLE);
                    tvRangeMax.setVisibility(View.VISIBLE);
                    tvRangeCurrent.setVisibility(View.VISIBLE);
                    showRange = true;
                } else {
                    skbrRange.setVisibility(View.GONE);
                    tvRangeMin.setVisibility(View.GONE);
                    tvRangeMax.setVisibility(View.GONE);
                    tvRangeCurrent.setVisibility(View.GONE);
                    showRange = false;
                }
            }
        });

        skbrRange.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                tvRangeCurrent.setText(currentString + progress);
                rangeCircle.setRadius(progress);
                hideOrShowMarkers(progress);
                range = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return rootView;
    }

    private void drawCircle(int range){
        rangeCircle = map.addCircle(new CircleOptions()
                .center(latLng)
                .radius(range)
                .strokeColor(Color.BLUE));
    }

    private void hideOrShowMarkers(int range){
        for(Marker marker : markers){
            if(SphericalUtil.computeDistanceBetween(latLng, marker.getPosition()) < range){
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }
    }

    private void placeMarkers(){
        int poiArraySize = poiDataList.size();
        double poiLat;
        double poiLong;
        markers = new ArrayList<>();
        for( int i = 0; i < poiArraySize; i++) {
            poiLat = Double.parseDouble((String) poiDataList.get(i).get("poiLat"));
            poiLong = Double.parseDouble((String) poiDataList.get(i).get("poiLong"));
            locationMarker = map.addMarker(new MarkerOptions().position(new LatLng(poiLat, poiLong)).visible(false));
            locationMarker.setTag(poiDataList.get(i).get("poiId"));
            locationMarker.setTitle((String) poiDataList.get(i).get("poiName"));
            markers.add(locationMarker);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState){
        if(map != null){
            outState.putParcelable("location", lastKnownLocation);
            outState.putParcelable("camera_location", map.getCameraPosition());
            super.onSaveInstanceState(outState);
        }
    }

    private void updateLocation(){
        if (map == null) {
            return;
        }
        try {
            if(checkLocationPermission()){
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                map.setMyLocationEnabled(false);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                lastKnownLocation = null;
            }
        } catch (SecurityException e){
            Log.e("Exception :%s ",e.getMessage());
        }
    }

    public boolean checkLocationPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            return true;
        }
        return false;
    }

    public void getLocationPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private void getDeviceLocation() {
        try {
            if (checkLocationPermission()) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(Task<Location> task) {
                        if (task.isSuccessful()) {
                            lastKnownLocation = task.getResult();
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(lastKnownLocation.getLatitude(),
                                            lastKnownLocation.getLongitude()), 15));
                        } else {
                            Log.d("Error", "Current location is null. Using defaults.");
                            Log.e("Error", "Exception: %s", task.getException());
                            map.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, 15));
                            map.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    @Override
    public void onResume(){
        mapView.onResume();
        super.onResume();

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
    }
}
