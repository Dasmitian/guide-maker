package project.app.mobile.guidemaker.guidesLists;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.createGuide.CreateGuideActivity;

public class MyGuidesListAdapter extends BaseAdapter implements ListAdapter {
    private Context context;
    private FileManager fileManager;
    private String guideTitleText;
    private String username;
    private ArrayList<HashMap> guideDataList;
    private LocalSqliteDb localDb;
    private ArrayList languagesList;

    public MyGuidesListAdapter(ArrayList<HashMap> guideDataList, Context context, String username){
        this.guideDataList = guideDataList;
        this.context = context;
        this.username = username;
        fileManager = new FileManager();
        localDb = new LocalSqliteDb(context,  "localDatabase.db");
    }

    @Override
    public int getCount() {
        return guideDataList.size();
    }

    @Override
    public String getItem(int position) {
        return guideDataList.get(position).get("guideTitle").toString();
    }

    @Override
    public long getItemId(int position) {
        Integer id = (Integer) guideDataList.get(position).get("guideId");
        Long longId = id.longValue();
        return longId;
    }



    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.my_guides_list, null);
        }

        final TextView guideTitle = view.findViewById(R.id.myGuideTitle);
        final Button btnDeleteGuide = view.findViewById(R.id.buttonDeleteGuide);
        final Button btnEditGuide = view.findViewById(R.id.buttonEditGuide);
        final Button btnTranslateGuide = view.findViewById(R.id.buttonTranslateGuide);

        guideTitleText = guideDataList.get(position).get("guideTitle").toString();
        guideTitle.setText(guideTitleText);

        btnDeleteGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final AlertDialog.Builder alertDialogDelete = new AlertDialog.Builder(context);
                alertDialogDelete.setTitle(context.getResources().getString(R.string.delete_guide));
                alertDialogDelete.setMessage(context.getResources().getString(R.string.delete_guide_message));
                alertDialogDelete.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        fileManager.deleteGuideFolder("GuideMaker", guideTitleText);
                        localDb.deleteGuideFromLocalDb(getGuideId(v), username);
                        btnDeleteGuide.setVisibility(View.INVISIBLE);
                        btnEditGuide.setVisibility(View.INVISIBLE);
                        guideTitle.setVisibility(View.INVISIBLE);
                        btnTranslateGuide.setVisibility(View.INVISIBLE);
                    }
                });
                alertDialogDelete.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                alertDialogDelete.show();
                notifyDataSetChanged();
            }
        });

        btnEditGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagesList = localDb.getGuideLanguagesFromLocalDb(getGuideId(v));
                FragmentManager fragmentManager = ((ListGuides) context).getSupportFragmentManager();
                GuideLanguageToEditFragment guideLanguageToEditFragment = GuideLanguageToEditFragment.newInstance(languagesList, getGuideId(v));
                guideLanguageToEditFragment.show(fragmentManager, "guideLanguagesList");
            }
        });

        btnTranslateGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                languagesList = localDb.getGuideLanguagesFromLocalDb(getGuideId(view));
                showDialog(view, position, languagesList);
            }
        });

        return view;
    }

    private void showDialog(View view, int position, ArrayList translatedLanguagesList){
        FragmentManager fragmentManager = ((ListGuides) context).getSupportFragmentManager();
        TranslateLanguagesListDialogFragment translateLanguagesListDialogFragment = TranslateLanguagesListDialogFragment.newInstance(getGuideId(view), getItem(position), translatedLanguagesList);
        translateLanguagesListDialogFragment.show(fragmentManager, "languagesListDialogFragment");
    }

    private int getGuideId(View view){
        View parentRow = (View) view.getParent();
        ListView listView = (ListView) parentRow.getParent();
        long longId = getItemId(listView.getPositionForView(parentRow));
        return (int) longId;
    }
}
