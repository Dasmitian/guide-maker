package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SharedPreferencesClass;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapImageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapImageFragment extends Fragment {

    private String mapImageName;
    private SharedPreferencesClass preferences;
    private String guideName;
    private FileManager fileManager;
    private Paint paint;
    private Canvas canvas;
    private Bitmap bitmap;

    private File mapFile;

    private int startX;
    private int startY;
    private int endX;
    private int endY;
    private boolean drawRect = false;

    private OnFragmentInteractionListener mListener;

    public MapImageFragment() {
        // Required empty public constructor
    }

    public static MapImageFragment newInstance(String mapImageName) {
        MapImageFragment fragment = new MapImageFragment();
        Bundle args = new Bundle();
        args.putString("mapImageName", mapImageName);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mapImageName = getArguments().getString("mapImageName");
        }
        preferences = new SharedPreferencesClass(getActivity());
        guideName = preferences.getGuideName();
        fileManager = new FileManager();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_map_image, container, false);
        rootView.setWillNotDraw(false);

        final ImageView mapImageView = rootView.findViewById(R.id.mapImageView);
        Button doneButton = rootView.findViewById(R.id.mapImageDoneButton);

        loadImage(mapImageView);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        Bitmap tempBitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
        canvas = new Canvas(tempBitmap);
        mapImageView.setImageDrawable(new BitmapDrawable(getResources(), tempBitmap));

        mapImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        drawRect = true;
                        startX = (int) motionEvent.getX();
                        startY = (int) motionEvent.getY();
                        rootView.invalidate();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        endX = (int) motionEvent.getX();
                        endY = (int) motionEvent.getY();
                        rootView.invalidate();
                        break;
                    case MotionEvent.ACTION_UP:
                        drawRect = false;
                        canvas.drawRect(startX, startY, endX, endY, paint);
                        rootView.invalidate();

                        break;
                    default:
                    return false;
                }
                return true;
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return rootView;
    }

    private void loadImage(ImageView imageView){
        mapFile = new File(Environment.getExternalStorageDirectory() + "/GuideMaker/" + guideName, mapImageName);
        int rotation = fileManager.getExifOrientation(mapFile.getAbsolutePath());
        bitmap = BitmapFactory.decodeFile(mapFile.getAbsolutePath());
        bitmap = (fileManager.loadImage(getActivity(), rotation, bitmap));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}


