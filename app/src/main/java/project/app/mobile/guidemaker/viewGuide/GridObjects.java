package project.app.mobile.guidemaker.viewGuide;

import androidx.fragment.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.ImageFragment;
import project.app.mobile.guidemaker.R;

public class GridObjects extends AppCompatActivity implements ObjectFragment.OnFragmentInteractionListener, View.OnClickListener{

    FrameLayout mainLayout;
    LinearLayout subLayout;
    GridLayout gridLayout;
    FrameLayout fragmentHolder;

    ImageView imageView;
    LinearLayout[] linearLayout;
    DisplayMetrics metrics;

    TextView textView;
    FileManager fileManager;
    FragmentManager fragmentManager = getSupportFragmentManager();

    String imageName;
    String originalGuideTitle;
    String guideTitle;
    String mapName;
    String language;
    int objectsCount;

    ArrayList<HashMap<String, String>> interiorObjectsDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_objects);

        mainLayout = findViewById(R.id.mainFrameLayout);
        subLayout = findViewById(R.id.subLinearLayout);
        fileManager = new FileManager();

        Bundle extras = getIntent().getExtras();

        interiorObjectsDataList = (ArrayList<HashMap<String, String>>) extras.getSerializable("interiorObjectDataList");
        guideTitle = extras.getString("guideTitle");
        originalGuideTitle = extras.getString("originalGuideTitle");
        language = extras.getString("language");
        int guideId = extras.getInt("guideId");

        objectsCount = interiorObjectsDataList.size();
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        gridLayout = new GridLayout(getApplicationContext());
        gridLayout.setId(999);
        if(interiorObjectsDataList.size() < 3){
            gridLayout.setRowCount(1);
        } else {
            gridLayout.setRowCount(interiorObjectsDataList.size() / 3);
        }
        gridLayout.setColumnCount(3);
        linearLayout = new LinearLayout[objectsCount];

        Thread thread = new Thread() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        createGrid();
                    }
                });
            }
        };
        thread.start();
        subLayout.addView(gridLayout);

    }

    private void createGrid() {
        for (int i = 0; i < objectsCount; i++) {
            linearLayout[i] = new LinearLayout(getApplicationContext());
            linearLayout[i].setOrientation(LinearLayout.VERTICAL);
            linearLayout[i].setId(i);
            linearLayout[i].setPadding(10, 10, 10, 10);
            linearLayout[i].setOnClickListener(this);
            imageView = new ImageView(getApplicationContext());
            imageName = interiorObjectsDataList.get(i).get("imageFileName");
            imageView.setBackgroundResource(0);
            imageView.setMinimumWidth(metrics.widthPixels / 3);
            imageView.setMinimumHeight(metrics.heightPixels / 3);
            imageView.setMaxWidth(metrics.widthPixels / 3);
            imageView.setMaxHeight(metrics.heightPixels / 3);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            setImageButtonImage(imageView);
            textView = new TextView(getApplicationContext());
            textView.setText(interiorObjectsDataList.get(i).get("objectName"));
            textView.setTextSize(10);
            textView.setTextColor(Color.BLACK);
            textView.setGravity(Gravity.CENTER);

            linearLayout[i].addView(imageView);
            linearLayout[i].addView(textView);

            gridLayout.addView(linearLayout[i]);
        }
    }

    private void setImageButtonImage(ImageView imageView){
        String imagePathName = Environment.getExternalStorageDirectory() + "/GuideMaker/" + originalGuideTitle + "/" + imageName;
        BitmapFactory.Options options;
        options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        int rotation = fileManager.getExifOrientation(imagePathName);
        Bitmap bitmap = BitmapFactory.decodeFile(imagePathName, options);
        imageView.setImageBitmap(fileManager.loadImage(getApplicationContext(), rotation, bitmap));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        final String objectImageName = interiorObjectsDataList.get(id).get("imageFileName");
        final Fragment fragment = new ImageFragment().newInstance(objectImageName, originalGuideTitle, id, null, null);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                fragmentManager.beginTransaction().add(R.id.mainFrameLayout, fragment, "gridActivity").addToBackStack(null).commit();
            }
        });
        thread.start();
    }

    public void showObject(int id){
        final String objectName = interiorObjectsDataList.get(id).get("objectName");
        final String objectDescription = interiorObjectsDataList.get(id).get("objectDescription");
        final String objectImageName = interiorObjectsDataList.get(id).get("imageFileName");
        final String objectAudioName = interiorObjectsDataList.get(id).get("audioFileName");
        String objectPinPosX = interiorObjectsDataList.get(id).get("pinPosX");
        String objectPinPosY = interiorObjectsDataList.get(id).get("pinPosY");
        String mapName = interiorObjectsDataList.get(id).get("mapName");
        Fragment fragment = new ObjectFragment().newInstance(objectName, objectDescription, objectImageName, objectAudioName, mapName, originalGuideTitle, language, objectPinPosX, objectPinPosY);
        fragmentManager.beginTransaction().add(R.id.fullScrenImageLayout, fragment).addToBackStack(null).commit();

    }

    @Override
    public void onBackPressed(){
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++){
                getSupportFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }
    }
}
