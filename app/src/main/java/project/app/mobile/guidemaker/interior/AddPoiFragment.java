package project.app.mobile.guidemaker.interior;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.ImageFragment;
import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.SelectMapDialogFragment;
import project.app.mobile.guidemaker.SharedPreferencesClass;


public class AddPoiFragment extends Fragment {

    private TextView tvAudioFile;
    private TextView tvObjectImage;

    private EditText etObjectName;
    private EditText etObjectDescription;

    private String objectName;
    private String audioFileName;
    private String imageFileName;
    private String description;
    private ArrayList<String> mapsNamesList;
    private String pinPosX;
    private String pinPosY;
    private String mapName;

    private FileManager fileManager = new FileManager();
    FragmentManager fragmentManager;

    private File audioFile;
    private File imageFile;
    private static int REQUEST_LOAD_IMAGE = 1;
    private static int REQUEST_LOAD_AUDIO = 2;
    private int position;
    private boolean editing;
    private String guideName;
    private String language;
    private SharedPreferencesClass preferences;
    private boolean imageSelected = false;
    private boolean audioSelected = false;

    OnObjectDataPass dataPasser;
    HashMap<String, String> objectDataTranslations;
    private static final String KEY_OBJECT_NAME = "objectName";
    private static final String KEY_OBJECT_AUDIO = "audioFileName";
    private static final String KEY_OBJECT_IMAGE = "imageFileName";
    private static final String KEY_OBJECT_DESC = "objectDescription";
    private static final String KEY_OBJECT_LANGUAGE = "language";
    private static final String KEY_OBJECT_PINX = "pinPosX";
    private static final String KEY_OBJECT_PINY = "pinPosY";
    private static final String KEY_MAP_NAME = "mapName";

    private String errorMessage;

    public interface OnObjectDataPass {
        void passObjectData(HashMap data, int position);
    }

    public static AddPoiFragment newInstance(Boolean editing, int position, HashMap objectData, String language, ArrayList<String> mapsNamesList){
        AddPoiFragment fragment = new AddPoiFragment();

        Bundle args = new Bundle();
        args.putBoolean("EDITING", editing);
        args.putSerializable("OBJECT_DATA", objectData);
        args.putInt("POSITION", position);
        args.putString("LANGUAGE", language);
        args.putStringArrayList("MAPS_NAMES_LIST", mapsNamesList);

        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_poi, container, false);
        // Inflate the layout for this fragment

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        tvAudioFile = rootView.findViewById(R.id.selectedAudioFile);
        tvObjectImage = rootView.findViewById(R.id.selectedObjectImage);
        etObjectName = rootView.findViewById(R.id.etObjectName);
        etObjectDescription = rootView.findViewById(R.id.etObjectDescription);

        errorMessage = getResources().getString(R.string.add_poi_error_message);

        preferences = new SharedPreferencesClass(getActivity());
        fragmentManager = getFragmentManager();
        guideName = preferences.getGuideName();

        final Button audioFileButton = rootView.findViewById(R.id.audioFileButton);
        final Button objectImageButton = rootView.findViewById(R.id.objectImageButton);
        Button addObjectButton = rootView.findViewById(R.id.addObjectButton);
        Button editObjectButton = rootView.findViewById(R.id.editObjectButton);
        Button placePinButton = rootView.findViewById(R.id.placePinButton);
        savedInstanceState = getArguments();
        language = savedInstanceState.getString("LANGUAGE");
        editing = savedInstanceState.getBoolean("EDITING");
        mapsNamesList = savedInstanceState.getStringArrayList("MAPS_NAMES_LIST");
        if( editing ){
            loadData(savedInstanceState);
            addObjectButton.setVisibility(View.GONE);
        } else {
            editObjectButton.setVisibility(View.GONE);
        }

        audioFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_AUDIO);
            }
        });

        objectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_IMAGE);
            }
        });

        addObjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getEtObjectName().equals("")){
                    Toast.makeText(getActivity(),getResources().getString(R.string.error_field_required), Toast.LENGTH_SHORT).show();
                } else {
                    if (getEtObjectDescription().equals("") && getTvAudioFile().equals("")) {
                        tvAudioFile.setError(errorMessage);
                        etObjectDescription.setError(errorMessage);
                    } else {
                        objectDataTranslations = new HashMap<>();
                        objectDataTranslations.put(KEY_OBJECT_NAME, getEtObjectName());
                        objectDataTranslations.put(KEY_OBJECT_AUDIO, getTvAudioFile());
                        objectDataTranslations.put(KEY_OBJECT_IMAGE, getTvObjectImageName());
                        objectDataTranslations.put(KEY_OBJECT_DESC, getEtObjectDescription());
                        objectDataTranslations.put(KEY_OBJECT_LANGUAGE, language);
                        objectDataTranslations.put(KEY_OBJECT_PINX, pinPosX);
                        objectDataTranslations.put(KEY_OBJECT_PINY, pinPosY);
                        objectDataTranslations.put(KEY_MAP_NAME, mapName);
                        passObjectData(objectDataTranslations, -1);
                        copyFiles();
                        etObjectName.setText("");
                        tvAudioFile.setText("");
                        tvObjectImage.setText("");
                        etObjectDescription.setText("");
                        mapName = null;
                        Toast.makeText(getActivity(), getResources().getString(R.string.object_added), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        editObjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editObjectData();
                passObjectData(objectDataTranslations, position);
                copyFiles();
                getActivity().onBackPressed();
            }
        });

        placePinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mapsNamesList.contains(mapName)){
                    Fragment fragment = new ImageFragment().newInstance(mapName, guideName, 0, null, null);
                    fragmentManager = getChildFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.addPoiFragment, fragment, "poiFragment").addToBackStack("selectMap").commit();
                } else if(mapsNamesList == null){
                    Toast.makeText(getContext(), getResources().getString(R.string.map_image_missing), Toast.LENGTH_SHORT).show();
                } else {
                    Fragment selectMapDialogFragment = new SelectMapDialogFragment().newInstance(mapsNamesList, guideName);
                    fragmentManager = getChildFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.addPoiFragment, selectMapDialogFragment, "dialogFragment").addToBackStack(null).commit();

                }
            }
        });

        return rootView;
    }

    public void passObjectData(HashMap data, int position){
        dataPasser.passObjectData(data, position);
    }

    private void copyFiles(){
        File image = new File(Environment.getExternalStorageDirectory() + "/GuideMaker/" + guideName, getTvObjectImageName());
        File audio = new File( Environment.getExternalStorageDirectory() + "/GuideMaker/" + guideName, getTvAudioFile());
        if(editing){
            if(imageSelected) {
                fileManager.deleteFile("/GuideMaker/", guideName, imageFileName);
            }
            if(audioSelected) {
                fileManager.deleteFile("/GuideMaker/", guideName, audioFileName);
            }
        }
        try {
            if(imageSelected) {
                fileManager.copyFile(imageFile, image);
            }
            if(audioSelected) {
                fileManager.copyFile(audioFile, audio);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == REQUEST_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            imageFile = new File(picturePath);
            tvObjectImage.setText(imageFile.getName());
            imageSelected = true;
        } else if(requestCode == REQUEST_LOAD_AUDIO && resultCode == Activity.RESULT_OK && null != data){
            Uri selectedAudio = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedAudio,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String audioPath = cursor.getString(columnIndex);
            audioFile = new File(audioPath);
            tvAudioFile.setText(audioFile.getName());
            audioSelected = true;
        }
    }

    private void loadData(Bundle savedInstanceState){
        objectDataTranslations = (HashMap<String, String>) savedInstanceState.getSerializable("OBJECT_DATA");
        objectName = objectDataTranslations.get(KEY_OBJECT_NAME);
        audioFileName = objectDataTranslations.get(KEY_OBJECT_AUDIO);
        imageFileName = objectDataTranslations.get(KEY_OBJECT_IMAGE);
        description = objectDataTranslations.get(KEY_OBJECT_DESC);
        pinPosX = objectDataTranslations.get(KEY_OBJECT_PINX);
        pinPosY = objectDataTranslations.get(KEY_OBJECT_PINY);
        mapName = objectDataTranslations.get(KEY_MAP_NAME);
        position = savedInstanceState.getInt("POSITION");
        language = savedInstanceState.getString("LANGUAGE");

        etObjectName.setText(objectName);
        tvAudioFile.setText(audioFileName);
        tvObjectImage.setText(imageFileName);
        etObjectDescription.setText(description);
    }

    private void editObjectData(){
        objectDataTranslations.put(KEY_OBJECT_NAME, getEtObjectName());
        objectDataTranslations.put(KEY_OBJECT_AUDIO, getTvAudioFile());
        objectDataTranslations.put(KEY_OBJECT_IMAGE, getTvObjectImageName());
        objectDataTranslations.put(KEY_OBJECT_DESC, getEtObjectDescription());
        objectDataTranslations.put(KEY_OBJECT_PINX, pinPosX);
        objectDataTranslations.put(KEY_OBJECT_PINY, pinPosY);
        objectDataTranslations.put(KEY_MAP_NAME, mapName);
    }

    public void setPinPosAndMapName(double pinX, double pinY, String mapFileName){
        pinPosX = String.valueOf(pinX);
        pinPosY = String.valueOf(pinY);
        mapName = mapFileName;
    }

    public void closeImage(){
        getChildFragmentManager().popBackStack();
    }

    public String getEtObjectName() {
        return etObjectName.getText().toString().trim();
    }

    public String getEtObjectDescription() {
        return etObjectDescription.getText().toString().trim();
    }

    public String getTvAudioFile(){
        return tvAudioFile.getText().toString().trim();
    }

    public String getTvObjectImageName(){
        return tvObjectImage.getText().toString().trim();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnObjectDataPass) context;
    }
}
