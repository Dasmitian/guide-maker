package project.app.mobile.guidemaker;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.io.File;

import project.app.mobile.guidemaker.interior.AddPoiFragment;
import project.app.mobile.guidemaker.viewGuide.GridObjects;

public class ImageFragment extends Fragment {

    private String imageName;
    private String imagePath;
    private String guideTitle;
    private String pinPosX;
    private String pinPosY;
    private int id;

    private FileManager fileManager = new FileManager();
    FragmentManager fragmentManager;
    ImageView fullScreenImage;
    Button btnView;
    Button btnDone;
    LinearLayout linearLayout;
    ImageView imageView;
    Bitmap imageBitmap;
    Display display;
    Point size;

    public ImageFragment() {
        // Required empty public constructor
    }

    public static ImageFragment newInstance(String objectImageName, String originalGuideTitle, int id, String pinPosX, String pinPosY){
        ImageFragment fragment = new ImageFragment();

        Bundle args = new Bundle();
        args.putString("IMAGE_NAME", objectImageName);
        args.putString("ORIGINAL_GUIDE_TITLE", originalGuideTitle);
        args.putInt("ID", id);
        args.putString("PIN_POS_X", pinPosX);
        args.putString("PIN_POS_Y", pinPosY);

        fragment.setArguments(args);
        return fragment;
    }

    public static ImageFragment newInstance(String mapName, String originalGuideTitle){
        ImageFragment fragment = new ImageFragment();

        Bundle args = new Bundle();
        args.putString("IMAGE_NAME", mapName);
        args.putString("ORIGINAL_GUIDE_TITLE", originalGuideTitle);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_image, container, false);
        savedInstanceState = getArguments();
        imageName = savedInstanceState.getString("IMAGE_NAME");
        guideTitle = savedInstanceState.getString("ORIGINAL_GUIDE_TITLE");
        id = savedInstanceState.getInt("ID");
        pinPosX = savedInstanceState.getString("PIN_POS_X");
        pinPosY = savedInstanceState.getString("PIN_POS_Y");

        display = getActivity().getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        fullScreenImage = rootView.findViewById(R.id.fullScreenImage);
        btnView = rootView.findViewById(R.id.buttonShowObject);
        btnDone = rootView.findViewById(R.id.buttonDone);
        btnView.setVisibility(View.INVISIBLE);
        btnDone.setVisibility(View.INVISIBLE);
        if(getActivity().getClass().getSimpleName().equals("GridObjects")){
            btnView.setVisibility(View.VISIBLE);
        } else {
            btnView.setVisibility(View.INVISIBLE);
        }

        if(id==12345){
            btnView.setVisibility(View.INVISIBLE);
        }

        if(getActivity().getClass().getSimpleName().equals("InteriorActivity")){
            btnDone.setVisibility(View.VISIBLE);
        } else {
            btnDone.setVisibility(View.INVISIBLE);
        }

        fragmentManager = getFragmentManager();

        imagePath = Environment.getExternalStorageDirectory() + "/" + "GuideMaker" + "/" + guideTitle + "/" + imageName;
        int imageRotation = fileManager.getExifOrientation(imagePath);
        File imageFile = new File(imagePath);
        imageBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        fullScreenImage.setImageBitmap(fileManager.loadImage(getActivity(), imageRotation, imageBitmap));

        fullScreenImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(fragmentManager.findFragmentByTag("poiFragment") != null || fragmentManager.findFragmentByTag("dialogFragment") != null) { //prevent touch event when viewing image outside AddPoiFragment
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        placePin(motionEvent);
                    }
                }
                    return true;

            }
        });

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(id != 12345){
                    ((GridObjects)getActivity()).showObject(id);
                }
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getParentFragment().getTag().equals("poiFragment")) {
                    ((AddPoiFragment) getParentFragment()).closeImage();
                } else {
                    ((SelectMapDialogFragment) getParentFragment()).closeImage();
                }
            }
        });

        if(fragmentManager.findFragmentByTag("objectFragment") != null){
            if(fullScreenImage != null) {
                showPin(rootView);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.map_image_missing), Toast.LENGTH_SHORT).show();
            }
        }


        return rootView;
    }

    private void showPin(View rootView){
        linearLayout = rootView.findViewById(R.id.pinLayout);
        imageView = new ImageView(getContext());
        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.gps);
        imageView.setImageBitmap(bitmap1);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        int pinX = (int)(Double.parseDouble(pinPosX) * size.x) / 100;
        int pinY = (int)(Double.parseDouble(pinPosY) * size.y) / 100;
        params.leftMargin = pinX - bitmap1.getWidth()/2;
        params.topMargin = pinY - bitmap1.getHeight()/2;

        linearLayout.addView(imageView, params);
    }

    private void placePin(MotionEvent motionEvent){
        if(linearLayout != null){
            linearLayout.removeView(imageView);
        }
        linearLayout = getView().findViewById(R.id.pinLayout);
        imageView = new ImageView(getContext());

        double touchX = motionEvent.getX();
        double touchY = motionEvent.getY();

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.gps);
        imageView.setImageBitmap(bitmap1);

        //values in % of screen to allow showing pin on different resolution
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        double pinX = ((100 * touchX) / size.x);
        double pinY = ((100 * touchY) / size.y);
        params.leftMargin = (int) (touchX - bitmap1.getWidth()/2);
        params.topMargin = (int) (touchY - bitmap1.getHeight()/2);

        linearLayout.addView(imageView, params);
        if(getParentFragment().getTag().equals("poiFragment")) {
            ((AddPoiFragment) getParentFragment()).setPinPosAndMapName(pinX, pinY, imageName);
        } else {
            ((SelectMapDialogFragment) getParentFragment()).passPinAndMapName(pinX, pinY, imageName);
        }
    }

}
