package project.app.mobile.guidemaker.viewGuide;

import android.net.Uri;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.R;

public class ListObjects extends AppCompatActivity implements ObjectFragment.OnFragmentInteractionListener, CommentFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_objects);

        Bundle extras = getIntent().getExtras();

        ArrayList<HashMap<String, String>> interiorObjectsDataList = (ArrayList<HashMap<String, String>>) extras.getSerializable("interiorObjectDataList");
        String guideTitle = extras.getString("guideTitle");
        String originalGuideTitle = extras.getString("originalGuideTitle");
        String language = extras.getString("language");
        int guideId = extras.getInt("guideId");

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        int listSize = interiorObjectsDataList.size();
        for (int i = 0; i < listSize; i++) {
            tabLayout.addTab(tabLayout.newTab());
        }
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.comment_guide)));
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        int tabCount = tabLayout.getTabCount();

        final ViewPager viewPager = findViewById(R.id.viewPagerObject);
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), tabCount, interiorObjectsDataList, originalGuideTitle, guideId, language);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    @Override
    public void onFragmentInteraction(Uri uri){

    }
}
