package project.app.mobile.guidemaker.createGuide;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.toolbox.JsonObjectRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public interface CreateGuideContract {

    interface CreateGuideView {
        void initProgressDialog();
        void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
        void loadNavDrawerUser();
        String getTvSelectedLanguage();
        String getEtTitle();
        String getEtDescription();
        String getGuideType();
        String getImageName();
        void setEtTitleError(String message);
        boolean checkPermission();
        void showToastMessage(String message);
        void accessSingleton(JsonObjectRequest jsObjectRequest);
        void loadInterior();
    }

    interface CreateGuidePresenter {
        Bitmap loadImage(int rotation, Bitmap bitmap);
        int getExifOrientation(String imagePath);
        boolean isTitleEmpty(String title);
        boolean checkSaveStatus();
        boolean checkIfGuideExists(String title);
        void deleteGuideFolder(String folderName);
        boolean createGuideFolder(String guideTitle);
        void copyPreviewImage(File previewImage, String guideTitle);
        int handleNextButton(String guideTitle, int editedGuideId, boolean nextButtonClicked, boolean editing, String language);
        void setGuideData();
        int handleSaveButton(String guideTitle, File imageFile, boolean editing, int editedGuideId);
    }

    interface CreateGuidePresenterOps {
        void showToastMessage(String message);
        void onGuideExists(String message);
        void passJsObjectRequest(JsonObjectRequest jsObjectRequest);
    }

    interface CreateGuideModel {
        void addGuideToList(String title, String description, String guideType);
        void checkGuideTitleInDB(String title);
        void createTablesInLocalDb(Context context);
        String getUsername();
        String getGuideTitleFromLocalDb(int editedGuideId);
        String getPreviewImageNameFromLocalDb(int editedGuideId);
        HashMap getInteriorData();
        void setInteriorData(HashMap interiorData);
        ArrayList<HashMap> getInteriorObjectData();
        void setInteriorObjectData(ArrayList<HashMap> interiorObjectData, boolean itemsSwapped);
        void addGuideToLocalDb();
        void addInteriorToLocalDb();
        void addInteriorObjectsToLocalDb();
        void updateGuideInLocalDb(int editedGuideId);
        void updateInteriorInLocalDb(int editedGuideId);
        void updateInteriorObjectsInLocalDb(int editedGuideId);
        void setGuideData(String guideTitle, String imageName, String guideDescription, String guideType, String language);
    }
}
