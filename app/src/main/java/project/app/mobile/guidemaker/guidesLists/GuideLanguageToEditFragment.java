package project.app.mobile.guidemaker.guidesLists;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import project.app.mobile.guidemaker.R;
import project.app.mobile.guidemaker.createGuide.CreateGuideActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GuideLanguageToEditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GuideLanguageToEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuideLanguageToEditFragment extends DialogFragment {

    private ArrayList<String> availableLanguages;
    private String chosenLanguage;

    private OnFragmentInteractionListener mListener;

    public GuideLanguageToEditFragment() {
    }

    public static GuideLanguageToEditFragment newInstance(ArrayList<String> availableLanguages, int guideId) {
        GuideLanguageToEditFragment fragment = new GuideLanguageToEditFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("availableLanguages", availableLanguages);
        args.putInt("guideId", guideId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_guide_language_to_edit, container, false);

        availableLanguages = getArguments().getStringArrayList("availableLanguages");
        RadioGroup radioGroup = rootView.findViewById(R.id.editingLanguagesRadioGroup);
        Button nextButton = rootView.findViewById(R.id.editingLanguagesNextButton);
        createAvailableRadioButtons(radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = rootView.findViewById(i);
                chosenLanguage = radioButton.getText().toString();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(chosenLanguage != null) {
                    Intent i = new Intent(getActivity(), CreateGuideActivity.class);
                    i.putExtra("guideId", getArguments().getInt("guideId"));
                    i.putExtra("editing", true);
                    i.putExtra("language", chosenLanguage);
                    getActivity().startActivity(i);
                }
            }
        });

        return rootView;
    }

    private void createAvailableRadioButtons(RadioGroup radioGroup){
        int size = availableLanguages.size();
        RadioButton[] radioButton = new RadioButton[size];
        for(int i = 0; i < size; i++){
            radioButton[i] = new RadioButton(getActivity());
            radioButton[i].setId(i);
            radioButton[i].setText(availableLanguages.get(i));
            radioGroup.addView(radioButton[i]);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
