package project.app.mobile.guidemaker.register;

import android.content.Context;

import com.android.volley.toolbox.JsonObjectRequest;

import project.app.mobile.guidemaker.R;

public class RegisterPresenterImpl implements RegisterContract.RegisterPresenter, RegisterContract.RegisterPresenterOps {


    private static final String KEY_EMPTY = "";
    private Context context;

    private RegisterContract.RegisterView registerView;
    private RegisterContract.RegisterModel registerModel;

    public RegisterPresenterImpl(RegisterContract.RegisterView registerView, Context context){
        this.registerView = registerView;
        this.context = context;
        this.registerModel = new RegisterModelImpl(this, context);
    }

    @Override
    public void registerUser() {
        registerModel.register(registerView.getEtUsername(), registerView.getEtPassword(), registerView.getEtEmail());
    }

    @Override
    public int validateInputs(String username, String password, String email, String confirmPassword) {
        if(KEY_EMPTY.equals(username)){
            return 1;
        }
        if(KEY_EMPTY.equals(email)){
            return 2;
        }
        if(KEY_EMPTY.equals(password)){
            return 3;
        }
        if(KEY_EMPTY.equals(confirmPassword)){
            return 4;
        }
        if(!password.equals(confirmPassword)){
            return 5;
        }
        return 0;
    }

    @Override
    public void onRegisterSuccess(String message) {
        registerView.showToastMessage("ok");
        registerView.loadLoginActivity();
    }

    @Override
    public void onUsernameTaken(String message) {
        registerView.setEtUsername("taken");
    }

    @Override
    public void onRegisterFail() {
        registerView.showToastMessage(context.getResources().getString(R.string.error_missing_parameters));
    }
    @Override
    public void passJsObjectRequest(JsonObjectRequest jsObjectRequest){
        registerView.accessSingleton(jsObjectRequest);
    }
}
