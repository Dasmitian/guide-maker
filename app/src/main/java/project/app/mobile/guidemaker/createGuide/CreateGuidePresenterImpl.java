package project.app.mobile.guidemaker.createGuide;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.android.volley.toolbox.JsonObjectRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import project.app.mobile.guidemaker.FileManager;
import project.app.mobile.guidemaker.R;

public class CreateGuidePresenterImpl implements CreateGuideContract.CreateGuidePresenter, CreateGuideContract.CreateGuidePresenterOps {

    private static final String KEY_EMPTY = "";
    private boolean progressSaved = false;

    private String mainFolderName = "GuideMaker";
    private Context context;

    FileManager fileManager = new FileManager();

    private CreateGuideContract.CreateGuideView createGuideView;
    private CreateGuideContract.CreateGuideModel createGuideModel;

    public CreateGuidePresenterImpl(CreateGuideContract.CreateGuideView createGuideView, Context context){
        this.createGuideView = createGuideView;
        this.createGuideModel = new CreateGuideModelImpl(this, context);
        this.context = context;
    }

    @Override
    public int handleNextButton(String guideTitle, int editedGuideId, boolean nextButtonClicked, boolean editing, String language){
        if (isTitleEmpty(guideTitle)){
            return 1;
        }
        if( language.equals("") ){
            return 3;
        }
        if(!nextButtonClicked && !editing) {
            if (!checkIfGuideExists(guideTitle)) {
                createGuideFolder(guideTitle);
                return 0;
            } else {
                return 2;
            }
        }
        return 0;
    }

    private void changeFolderName(String guideTitle, int guideId){
        String oldGuideTitle = createGuideModel.getGuideTitleFromLocalDb(guideId);
        if(!guideTitle.equals(oldGuideTitle)) {
            if (checkIfGuideExists(guideTitle)) {
                createGuideView.showToastMessage(context.getResources().getString(R.string.guide_exists));
            } else {
                File oldGuideFolder = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName, oldGuideTitle);
                File guideFolder = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName, guideTitle);
                oldGuideFolder.renameTo(guideFolder);
            }
        }
    }

    @Override
    public int handleSaveButton(String guideTitle, File imageFile, boolean editing, int editedGuideId){
        setGuideData();
        if( editing ){
            changeFolderName(guideTitle, editedGuideId);
            if(!imageFile.getName().equals(createGuideModel.getPreviewImageNameFromLocalDb(editedGuideId))) {
                copyPreviewImage(imageFile, guideTitle);
                fileManager.deleteFile(mainFolderName, guideTitle, createGuideModel.getPreviewImageNameFromLocalDb(editedGuideId));
            }
            createGuideModel.updateGuideInLocalDb(editedGuideId);
            createGuideModel.updateInteriorInLocalDb(editedGuideId);
            createGuideModel.updateInteriorObjectsInLocalDb(editedGuideId);
        } else {
            createGuideModel.createTablesInLocalDb(context);
            createGuideModel.addGuideToLocalDb();
            createGuideModel.addInteriorToLocalDb();
            createGuideModel.addInteriorObjectsToLocalDb();
            if (imageFile != null) {
                copyPreviewImage(imageFile, guideTitle);
            }
        }
        progressSaved = true;
        return 0;
    }

    @Override
    public void setGuideData(){
        createGuideModel.setGuideData(createGuideView.getEtTitle(), createGuideView.getImageName(), createGuideView.getEtDescription(), createGuideView.getGuideType(), createGuideView.getTvSelectedLanguage());
    }

    @Override
    public boolean createGuideFolder(String guideTitle){
        File mainFolder = new File(Environment.getExternalStorageDirectory(), mainFolderName);
        if (!mainFolder.exists()) {
            mainFolder.mkdirs();
        }
        File guideFolder = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName, guideTitle);
        if (!guideFolder.exists()) {
            return guideFolder.mkdirs();
        }
        return true;
    }

    @Override
    public void copyPreviewImage(File previewImage, String guideTitle){
        File image = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName + "/" + guideTitle, previewImage.getName());
        try {
            fileManager.copyFile(previewImage, image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Bitmap loadImage(int rotation, Bitmap bitmap) {
        return fileManager.loadImage(context, rotation, bitmap);
    }

    @Override
    public int getExifOrientation(String imagePath) {
        int rotation = fileManager.getExifOrientation(imagePath);
        return rotation;
    }

    @Override
    public void deleteGuideFolder(String folderName){
        fileManager.deleteGuideFolder(mainFolderName, folderName);
    }

    public void setInteriorData(HashMap interiorData) {
        createGuideModel.setInteriorData( interiorData);
    }

    public HashMap getInteriorData() {
        return createGuideModel.getInteriorData();
    }

    public void setInteriorObjectData(ArrayList<HashMap> interiorObjectData, boolean itemsSwapped) {
        createGuideModel.setInteriorObjectData(interiorObjectData, itemsSwapped);
    }

    public ArrayList<HashMap> getInteriorObjectData() {
        return createGuideModel.getInteriorObjectData();
    }

    @Override
    public boolean checkSaveStatus(){
        return progressSaved;
    }

    @Override
    public boolean checkIfGuideExists(String title){
        File guideFolder = new File(Environment.getExternalStorageDirectory() + "/" + mainFolderName, title);
        if (guideFolder.exists()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isTitleEmpty(String title) {
        return KEY_EMPTY.equals(title);
    }

    @Override
    public void showToastMessage(String message) {
        createGuideView.showToastMessage(message);
    }

    @Override
    public void onGuideExists(String message) {
        createGuideView.setEtTitleError(message);
    }

    @Override
    public void passJsObjectRequest(JsonObjectRequest jsObjectRequest){
        createGuideView.accessSingleton(jsObjectRequest);
    }
}
