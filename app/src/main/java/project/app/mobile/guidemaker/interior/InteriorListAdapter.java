package project.app.mobile.guidemaker.interior;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import project.app.mobile.guidemaker.LocalSqliteDb;
import project.app.mobile.guidemaker.R;

public class InteriorListAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list;
    private Context context;

    public InteriorListAdapter(ArrayList<String> list, Context context){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if ( view == null ){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.interior_list, null);
        }

        TextView itemsList = view.findViewById(R.id.item_list);
        itemsList.setText(list.get(position));

        Button deleteButton = view.findViewById(R.id.delete_button);
        Button editButton = view.findViewById(R.id.edit_button);
        ImageButton upButton = view.findViewById(R.id.up_button);
        ImageButton downButton = view.findViewById(R.id.down_button);

        if(list.indexOf(list.get(position)) == 0){
            upButton.setVisibility(View.INVISIBLE);
        } else {
            upButton.setVisibility(View.VISIBLE);
        }

        if(list.indexOf(list.get(position)) == getCount() - 1){
            downButton.setVisibility(View.INVISIBLE);
        } else {
            downButton.setVisibility(View.VISIBLE);
        }

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InteriorActivity)context).deleteObjectFromArray(position);
                list.remove(position);
                notifyDataSetChanged();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InteriorActivity)context).editPoiFragment(position);
            }
        });

        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collections.swap(list, list.indexOf(list.get(position)), list.indexOf(list.get(position - 1)));
                ((InteriorActivity)context).swapItemsInArray(list.indexOf(list.get(position)), list.indexOf(list.get(position - 1)));
                notifyDataSetChanged();
            }
        });

        downButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collections.swap(list, list.indexOf(list.get(position)), list.indexOf(list.get(position + 1)));
                ((InteriorActivity)context).swapItemsInArray(list.indexOf(list.get(position)), list.indexOf(list.get(position + 1)));
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
